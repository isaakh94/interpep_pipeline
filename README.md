# README #

Welcome to InterPep2, a template-based global peptide docking method.

Note that while InterPep2 itself is free for all, some of its dependencies are only free for academic purposes.

If you are looking for InterPep1, the peptide-protein interaction-site predictor, please checkout the branch named "InterPep1.0".

### SETUP ###

The first thing you want to do is make sure you do not have any "databases/paths_to_partners" or "databases/interfaces_full_list" files. The first time you run InterPep2, these files will be created depending on the content of your local PDB and Interface libraries. Whenever you update your library of Interfaces, or your PDB, these files should be removed and they will update as well. If you update your PDB, I recommend also updating your Interfaces library.

InterPep2 relies on a list of common dependencies. To set up InterPep2 on your machine, you will need the following:

*  Make sure you have python 2 installed, with scikitlearn and scipy.
*  Make sure the following databases are available on your machine:
    *  InterComp interface database. Download InterComp at wallnerlab.org/InterComp and follow the setup-instructions.
    *  The PDB Biounit coordinates, with entries in .pdb format. The PDB and instructions for download is available from wwpdb.org/ftp/pdb-ftp-sites
    *  Uniref90 fasta file, available at uniprot.org/downloads
    *  UniprotKB clustered for use with HHSUITE, including reviewed and unreviewed sequences, available at http://wwwuser.gwdg.de/~compbiol/uniclust/
*  Make sure you have the following softwares installed on your machine:
    *  InterComp interface alignment software. See databases above.
    *  BLAST sequence alignment software (NOT BLAST+), available at ftp://ftp.ncbi.nlm.nih.gov/blast/executables/legacy.NOTSUPPORTED/
    *  PSIPRED, available at bioinf.cs.ucl.ac.uk/software_downloads
    *  HHSUITE, primarily hhblits for multiple sequence alignment, available at github.com/soedinglab/hh-suite
    *  NACCESS, for solvent accessible area calculations, available at wolf.bms.umist.ac.uk/naccess
    *  Rosetta Software Suite, protein modeling software, available at rosettacommons.org/software/license-and-download
*  Read the configure.py file and set the paths to the dependecy softwares and databases there.
*  Use formatdb from BLAST to format the downloaded Uniref90 fasta file.
*  In the InterPep.py file, there is a line which sets the "BASE" of the software by setting an environment variable by `os.environ["BASE"]`. This is supposed to point to wherever InterPep2 is located on your system (the directory containing the InterPep.py file). If you are not using a cluster, setting the path to `os.path.dirname(os.path.realpath(__file__))` is fine, however you may have to set this manually.

### Test-set ###

If you want to run on targets from the sets used in the InterPep2 article, they
can be found at: http://bioinfo.ifm.liu.se/interpep/

### Running InterPep2 ###

InterPep2 takes as input a receptor structure in PDB format, as well as a peptide sequence in fasta format. The peptide sequence should have a header as the first row (starting with ">") and the whole sequence on the second row.

Running InterPep2 in the most basic way possible:

`python InterPep2.py receptor.pdb peptide.fa`

Running on 32 cores, comparing to a native structure, and placing results in "output/testtmp" directory:

`python InterPep2.py receptor.pdb peptide.fa -c 32 -n native.pdb -o output/testtmp`

Running only the final prediction step, which relies on all other steps having been completed previously:

`python InterPep2.py receptor.pdb peptide.fa -s 5`

### Questions? ###

Please direct any questions about the program to Isak Johansson (isak.johansson-akhe@liu.se)
