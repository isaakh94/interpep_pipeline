#!/bin/bash

pdb=$1
echo $pdb

chain1=$2
echo $chain1

chain2=$3
echo $chain2

sc_path=$4
echo $sc_path

$sc_path/sc XYZIN $pdb <<eof 
MOLECULE 1
CHAIN $chain1
MOLECULE 2
CHAIN $chain2
END
eof
