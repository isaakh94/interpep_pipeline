#!/usr/bin/env python

#feature_optimizer_latest_predictions.csv

import argparse
import numpy as np
import pandas as pd
import os
from fragment_feature_generator import superposition, make_aligned_complex, run_DockQ
import tempfile
import sys
import time
import math

def main():
    parser = argparse.ArgumentParser(description = 'This script requires you have used 2 separate forests for getting the data. First, use it with the -f flag on the 100,000 samples evaluated with the forest trained on 10,000 structures per training target. Then, use the script without this flag with the forest trained on 7,500 structures per training target.')
    parser.add_argument('target', type = str, help = 'searchpath to target protein')
    parser.add_argument('peptide_fragments', type = str, help = 'searchpath to folder containing peptide structures')
    parser.add_argument('predictions', type = str, help = 'searchpath to output from predictor')
    parser.add_argument('-f', '--first', action = 'store_true', help = 'Use this flag for the top hit. (initializing the output)')
    parser.add_argument('--no_ban', action = 'store_true', help = 'Set if you are not benchmarking')
    parser.add_argument('-b', '--banned', type = str, default = '', help = 'Set if you have another location for banned files')
    parser.add_argument('-c', '--cutoff_ban', type = float, default = 0.001, help = 'Set if you want another BLAST E-value cutoff for the ban rather than 0.001')
    parser.add_argument('-l', '--limit', type = float, default = 4.0, help = 'Similarity limit between predictions (LRMS)')
    parser.add_argument('-n', '--num', type = int, default = 10, help = 'How many predictions to make in total')
    parser.add_argument('-a', '--allow_similar', action = 'store_true', help = 'Removes restrictions on predicted structures being too similar. Normal behaviour is that predicted structures should not be from same receptor template nor within 4.0 Angstrom of any other prediction.')
    parser.add_argument('-o', '--output', type = str, default = '', help = 'Name of output-directory')
    args = parser.parse_args()
    
    start_time = time.time()
    
    if args.peptide_fragments[-1] == '/':
        args.peptide_fragments = args.peptide_fragments[:-1]

    #Loading up enviromental variables
    PDB = "/proj/wallner/users/x_clami/PDB/19052016_biounit"
    TMP = 'testtmp'
    TMP = os.environ['SNIC_TMP']
    SCRIPTS = "/proj/wallner/users/x_isaak/pepip_pipeline/scripts"
    COMPDB = "/proj/wallner/users/x_clami/interface_comparison/interface_comparison_nullmatching/interface_comparison/interfaces_bio_models_5"
    COMPDBDOMAIN = "/proj/wallner/users/x_isaak/interface_comparison/interfaces_bio_domain_5"
    OUTPUT = os.path.basename(args.target)[:4] + os.path.basename(args.peptide_fragments)[4] + os.path.basename(args.target)[4] + "_structures"
    if args.output:
        OUTPUT = args.output
    os.system('mkdir ' + OUTPUT)
    DOCKQ = "/proj/wallner/users/x_isaak/DockQ"
    
    #Loading prediction
    print 'pre-loading'
    sys.stdout.flush()
    data = pd.read_csv(args.predictions)
    print 'post-loading'
    sys.stdout.flush()
    data = data[data['0_target'] == os.path.basename(args.target)]
    data = data[data['link_density'] > 0.0]
    data.replace('None', np.nan, inplace = True)
    data.fillna(value=-100, inplace = True)

    if not args.no_ban:
        #Removing too-easy templates (for benchmarking)
        #"""
        banned_templates = load_banned_templates([os.path.basename(args.target)[:4], None, os.path.basename(args.target)[4]], args.banned, args.cutoff_ban)
        ban_list = [True if not j[:4] in banned_templates else False for i, j in enumerate(data['1_interface'])]
        data = data[ban_list]
        #"""
    
    #Sorting predictions
    data = data.sort_values(by = 'PREDICTED_DockQ-score', ascending = False)
    """
    def is_domain(row):
        if row['1_interface'].split('_')[-1] in ['d', 'd.pdb']:
            return 1
        else:
            return 0
    data['is_domain'] = data.apply(is_domain, axis = 1)
    data = data[data['is_domain'] == 1]
    """
    #data = data.sort_values(by = 'FACIT_DockQ-score', ascending = False)
    data = data.reset_index(drop = True)
    #data = data.head(100)
    
    predicted_complexes = []
    used_templates = []
    used_fragments = []
    final_predictions = pd.DataFrame(columns = data.columns.tolist() + ['3_model'])
    if not args.first:
        try:
            final_predictions = pd.read_csv(OUTPUT + '/predictions.csv').head(1)
            predicted_complexes = [OUTPUT + '/' + 'model_' + str(len(predicted_complexes)+1) + '.pdb']
            used_templates = final_predictions['1_interface'].tolist()
            used_fragments = final_predictions['2_fragment'].tolist()
        except Exception as e:
            print 'Something went wrong with loading of previous data! ERROR:', e
    for index, row in data.iterrows():
        if index >= args.num*1000 or time.time()-start_time >= 60*60*3:#60*60*24:
            break
        print row['1_interface'], row['2_fragment']
        if (row['1_interface'] in used_templates and not args.allow_similar) or row['clashes'] >= 10:# or row['2_fragment'] in used_fragments:
            continue
        interaction_complex = produce_structure(args.target, row['1_interface'], args.peptide_fragments + '/' + row['2_fragment'], PDB, TMP, SCRIPTS, COMPDB, COMPDBDOMAIN)
        #os.system('mv ' + interaction_complex + ' ' + interaction_complex + '.pdb')
        #interaction_complex = interaction_complex + '.pdb'
        LRMS = 0.0
        list_to_test_against = reversed(predicted_complexes)
        if args.allow_similar:
            list_to_test_against = []
        for comp in list_to_test_against:
            try:
                LRMS = float(run_DockQ(DOCKQ, interaction_complex, comp)[-1])
            except Exception as e:
                print 'DOCKQ-ERROR: ', e
                LRMS = 0.0
            print comp, LRMS
            if LRMS <= args.limit:
                os.system('rm ' + interaction_complex)
                break
        if os.path.isfile(interaction_complex):
            print 'STRUCTURE NR {} PREDICTED'.format(len(predicted_complexes)+1)
            sys.stdout.flush()
            final_predictions.loc[len(predicted_complexes)] = row.set_value('3_model', 'model_' + str(len(predicted_complexes)+1) + '.pdb')
            used_templates.append(row['1_interface'])
            #used_fragments.append(row['2_fragment'])
            new_complex = OUTPUT + '/' + 'model_' + str(len(predicted_complexes)+1) + '.pdb'
            os.system('mv ' + interaction_complex + ' ' + new_complex)
            predicted_complexes.append(new_complex)
            if len(predicted_complexes) >= args.num or args.first:
                break
    final_predictions.to_csv(OUTPUT + '/predictions.csv', index = False)

def produce_structure(target, interface, peptide, PDB, TMP, SCRIPTS, COMPDB, COMPDBDOMAIN):
    domain_interface = False
    if interface.split('_')[-1] in ['d', 'd.pdb']:
        domain_interface = True
    #Fetching interface origin
    origin_structure = tempfile.NamedTemporaryFile(delete = False, dir = TMP)
    origin_structure.close()
    command = 'grep -e "^MODEL" -e "^.\{15\}.\{6\}' + interface[7] + '" -e "^.\{15\}.\{6\}' + interface[9] + '" ' + PDB + '/' + interface[1:3] + '/' + interface[:5] + '.pdb >' + origin_structure.name
    os.system(command)
    #Making alignments
    if not domain_interface:
        positioned_target = superposition(target, origin_structure.name, interface[7], interface[6], SCRIPTS, TMP)
    else:
        positioned_target = superposition(target, origin_structure.name, interface[7], '1', SCRIPTS, TMP)
    if not domain_interface:
        alignment_output, interaction_complex = make_aligned_complex(os.path.abspath(positioned_target), interface, os.path.abspath(peptide), SCRIPTS, TMP, COMPDB)
    else:
        alignment_output, interaction_complex = make_aligned_complex(os.path.abspath(positioned_target), interface, os.path.abspath(peptide), SCRIPTS, TMP, COMPDBDOMAIN)
    #Cleaning up
    os.system('rm ' + positioned_target)
    os.system('rm ' + origin_structure.name)
    return interaction_complex

def load_banned_templates(target, ban_list_location, ban_cutoff):
    if ban_cutoff == 0.0:
        banned_templates = [target[0][:4]]
        return banned_templates
    ban_list_name = ban_list_location
    if ban_list_location == '':
        ban_list_location = 'final_blastresults.txt'
    banned_templates = []
    with open(ban_list_location, 'r') as f:
        banned_templates = f.readlines()
    banned_templates = [line for line in banned_templates if line.split()[0] == target[0] + '_' + target[2]]
    banned_templates = [line.split()[1][:4] for line in banned_templates if float(line.split()[-2]) <= ban_cutoff]
    return banned_templates

if __name__ == '__main__':
    main()
