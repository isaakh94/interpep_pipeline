#!/usr/bin/env python
import argparse
import os
import re
import numpy as np
from itertools import izip_longest
import operator
from scipy.cluster.hierarchy import dendrogram, linkage, fcluster
import commands
import sys
from math import factorial
from glob import glob

def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    """Smooth (and optionally differentiate) data with a Savitzky-Golay filter.
    The Savitzky-Golay filter removes high frequency noise from data.
    It has the advantage of preserving the original shape and
    features of the signal better than other types of filtering
    approaches, such as moving averages techniques.
    Parameters
    ----------
    y : array_like, shape (N,)
        the values of the time history of the signal.
    window_size : int
        the length of the window. Must be an odd integer number.
    order : int
        the order of the polynomial used in the filtering.
        Must be less then `window_size` - 1.
    deriv: int
        the order of the derivative to compute (default = 0 means only smoothing)
    Returns
    -------
    ys : ndarray, shape (N)
        the smoothed signal (or it's n-th derivative).
    Notes
    -----
    The Savitzky-Golay is a type of low-pass filter, particularly
    suited for smoothing noisy data. The main idea behind this
    approach is to make for each point a least-square fit with a
    polynomial of high order over a odd-sized window centered at
    the point.
    Examples
    --------
    t = np.linspace(-4, 4, 500)
    y = np.exp( -t**2 ) + np.random.normal(0, 0.05, t.shape)
    ysg = savitzky_golay(y, window_size=31, order=4)
    import matplotlib.pyplot as plt
    plt.plot(t, y, label='Noisy signal')
    plt.plot(t, np.exp(-t**2), 'k', lw=1.5, label='Original signal')
    plt.plot(t, ysg, 'r', label='Filtered signal')
    plt.legend()
    plt.show()
    References
    ----------
    .. [1] A. Savitzky, M. J. E. Golay, Smoothing and Differentiation of
       Data by Simplified Least Squares Procedures. Analytical
       Chemistry, 1964, 36 (8), pp 1627-1639.
    .. [2] Numerical Recipes 3rd Edition: The Art of Scientific Computing
       W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
       Cambridge University Press ISBN-13: 9780521880688
    """
    
    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError, msg:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve( m[::-1], y, mode='valid')

def extract_info(list_of_lines):
    heat_info = []
    anisou_info = []
    
    get_long = re.compile('\d+\.\d+')
    hit = False
    for line in list_of_lines:
        if line[:4] == 'ATOM' and line[16] in 'A ' and line[13:15] == 'CA':
            heat_info.append(float(re.search(get_long, line[60:67]).group()))
            hit = True
        elif line[:6] == 'ANISOU' and hit == True:
            anisou_info.append([int(i) for i in line[30:71].split()])
            hit = False
            
    heat_info = [1 if i > 0.01 else 0 for i in heat_info]
    return heat_info, anisou_info

def fill_5_float(inputfloat):
    outputstr = '%.2f' % inputfloat
    if len(outputstr) < 5:
        outputstr = ' ' + outputstr
    return outputstr

def fill_4_float(inputfloat):
    outputstr = '%.1f' % inputfloat
    if len(outputstr) < 4:
        outputstr = ' ' + outputstr
    return outputstr

def make_line_from_20(inputlist):
    return ' '.join([fill_4_float(i) for i in inputlist])

def pdb_printer(heat_list, anisou_list, start_file, score):
    f = open(start_file, 'r')
    lines = f.readlines()
    f.close()
    
    to_print_lines = ['HEADER    PePIP Score: ' + str(1/(0.2625/max(heat_list) + 0.7375/score)) + '\n']
    to_print_lines = to_print_lines + ['HEADER    Best Cluster Score: ' + str(score) + '\n']
    index = -1
    for line in lines:
        if line[:4] == 'ATOM':
            if line[16] in 'A ':
                index += 1
                to_print_lines.append(line[:61] + fill_5_float(heat_list[index]) + line[66:])
            else:
                to_print_lines.append(line[:61] + fill_5_float(heat_list[index]) + line[66:])
        elif line[:6] == 'ANISOU':
            if len(anisou_list[index]) > 0:
                to_print_lines.append(line[:29] + make_line_from_20(anisou_list[index]) + '\n')
            else:
                to_print_lines.append(line[:29] + make_line_from_20([0.0 for i in range(20)]) + '\n')
        else:
            to_print_lines.append(line)
    
    return ''.join(to_print_lines)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('predictions_file', type = str, help = 'searchpath to file containing all the predictions')
    parser.add_argument('feature_folder', type = str, help = 'searchpath to folder containing the features (no ending slash)')
    args = parser.parse_args()

    print args.feature_folder[:-15] + ' WORKING'
    
    #Reading Random Forest Output
    predictions = {}
    with open(args.predictions_file, 'r') as predictions_file:
        for line in predictions_file.readlines():
            predictions[line.split()[1]] = float(line.split()[0])
    
    ############### This section eliminates hits to too related structures ################
    # Under reconstruction
    """
    f = open('./final_blastresults.txt', 'r')
    blastlines = f.readlines()
    f.close()
    match_dict = {}
    current_target = blastlines[0].split()[0].replace('_', '')
    tiny_dict = {}
    for line in blastlines:
        data = line.split()
        data = [data[0].replace('_', ''), data[1].replace('_', '')] + [float(de) for de in data[2:]]
        if not data[0] == current_target:
            match_dict[current_target] = tiny_dict
            current_target = data[0]
            tiny_dict = {}
        if data[10] < 0.001: # <----------------------- Here you change the E-value cutoff
            tiny_dict[data[1]] = data[2:]
    match_dict[current_target] = tiny_dict
    del blastlines, current_target, tiny_dict

    new_ids_to_read = []
    new_predictions = []
    #if args.name in match_dict.keys():
    if True:
        for i in range(len(ids_to_read)):
            #if not ids_to_read[i][1] in match_dict[args.name].keys():
            if not ids_to_read[i][1] in match_dict['1kcrL'].keys():
                new_ids_to_read.append(ids_to_read[i])
                new_predictions.append(predictions[i])
    ids_to_read = new_ids_to_read
    predictions = new_predictions
    del new_ids_to_read, new_predictions
    """
    #######################################################################################
    
    #Getting a dictionary of the feature-files
    feature_files_list = glob(args.feature_folder + '/*.pdb')
    feature_files = {}
    pattern = re.compile('__([1-9][a-z0-9][a-z0-9][a-z0-9][\w]).pdb')
    for feature_file in feature_files_list:
        feature_files[re.search(pattern, feature_file).group(1)] = feature_file
    
    #Reading and preparing feature data
    surfsum = []
    acidsum = []
    weightsum = []
    for identity in predictions.keys():
        with open(feature_files[identity], 'r') as f:
            surface, acids = extract_info(f.readlines())
            surfsum.append(surface)
            acidsum.append(acids)
        weightsum.append(predictions[identity])
        
    ######################### CLUSTERING ##################################################
    
    SCRIPTS = os.environ["SCRIPTS"]
    
    #Getting distances between resides
    (contact_map_status, contact_map_output) = commands.getstatusoutput(SCRIPTS + '/' + 'contact_map_chain ' + feature_files[feature_files.keys()[0]])
    dist_list = [[float(char) for char in line.split()[5:]] for line in contact_map_output.split('\n')[2:]]
    
    #Clustering
    tempsurf = [list(np.where(np.array(su) == 1)[0]) for su in surfsum] #This step is required for the distance function to work swiftly
    
    def get_surface_distance(first, second, distances): #This is my distance function
        try:
            temp = []
            for i in first:
                for j in second:
                    temp.append(distances[i][j])
                    
            return sum(temp)/len(temp)
            
        except Exception as e:
            print str(e)
            return 100000000.0
    
    comparison_matrix = np.zeros((len(tempsurf) * (len(tempsurf) - 1)) // 2, dtype=np.double)
    position = 0
    for i in range(len(tempsurf) - 1):
        for j in range(i + 1, len(tempsurf)):
            comparison_matrix[position] = get_surface_distance(tempsurf[i], tempsurf[j], dist_list)
            position += 1
    del position
    
    Z = linkage(comparison_matrix, method='average') #Z contain the hierarchical clusters
    
    #######################################################################################
    
    #Picking cluster depth cutoff through elbow method
    last = savitzky_golay(Z[:, 2], 11, 3)
    last_rev = last[::-1]
    idxs = np.arange(1, len(last) + 1)
    acceleration = np.diff(last, 2)
    acceleration_rev = acceleration[::-1]
    cutoff = last_rev[acceleration_rev[:len(acceleration_rev)/2].argmax() + 2]

    #Showing dendrogram over surface distances
    #import matplotlib.pyplot as plt
    #plt.plot(idxs, last_rev)
    #plt.plot(idxs[:-2] + 1, acceleration_rev*10)
    #plt.show()
    #dendrogram(Z)
    #plt.axhline(y=cutoff, c='k')
    #plt.show()
    
    #Producing final clusters
    clusters = fcluster(Z, cutoff, criterion = 'distance')
    cluster_weights = [[] for i in range(max(clusters))]
    clusters = clusters - 1
    print(clusters)
    
    #Finding cluster with heaviest scoring interfaces. The weight of the top scorers within a cluster is averaged to mitigate outliers
    for ident in range(len(clusters)):
        cluster_weights[clusters[ident]].append(weightsum[ident])
    #cluster_weights = [np.average(sorted(clust_weight)[-min(1, len(clust_weight)):]) for clust_weight in cluster_weights]
    cluster_weights = [max(clust_weight) for clust_weight in cluster_weights]
    
    heavyness_list = []
    for scorer in range(len(cluster_weights)):
        heavyness_list.append([scorer, cluster_weights[scorer]])
    heavyness_list = sorted(heavyness_list, key = lambda x: x[-1], reverse=True)
    
    print cluster_weights
    
    #Producing final Surface and AA-interaction lists by weighted summation of predictions in heaviest cluster
    
    OUTPUT = os.environ["OUTPUT"]
    
    successful_predictions = 0
    for clusternumber in range(len(heavyness_list)):
        pepip_score = heavyness_list[clusternumber][1]
        heaviest = heavyness_list[clusternumber][0]
        finalsurf = []
        finalaa = []
        finalweights = []
        for num in range(len(surfsum)):
            if clusters[num] == heaviest:
                finalsurf.append(surfsum[num])
                finalaa.append([[j*10 for j in i] for i in acidsum[num]])
                finalweights.append(weightsum[num])
        top_weights = sorted(finalweights)[-int(max(1, float(len(finalweights))*0.10))]
        finalweights = [weight if weight >= top_weights else 0.0 for weight in finalweights]
        finalsurf = np.transpose(np.asarray(finalsurf))
        finalsurf = [np.average(finalsurf[i], weights=finalweights) for i in range(len(finalsurf))]
        finalaa = np.asarray(finalaa)
        finalaa = np.average(finalaa, weights=finalweights, axis = 0)
        if sum([1 for i in finalsurf if i >= 0.05]) >= 3:
            printable_string = pdb_printer(finalsurf, finalaa, feature_files[feature_files.keys()[0]], pepip_score)
            f = open(OUTPUT + '/' + os.path.basename(args.feature_folder).replace('_featfolder', '_PePIP_' + str(clusternumber) + '_.pdb'), 'w')
            f.write(printable_string)
            f.close()
            successful_predictions += 1
        else:
            print 'Rejected surface with score ' + str(pepip_score) + ' (too small)'
        if clusternumber >= 10:
            if successful_predictions == 0:
                print 'Junk surfaces, no prediction available'
            break
    
if __name__ == '__main__':
    main()
