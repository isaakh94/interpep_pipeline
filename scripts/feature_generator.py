#!/usr/bin/env python
import argparse
import pandas as pd
import tempfile
import os
from subprocess import call, check_output
import multiprocessing
from functools import partial
import signal
import numpy as np
import math
import sys
import re

def get_composition(sequence, identifier):
    """
    Takes a sequence of aa or ss and returns the relative composition of this sequence
    as a sorted list with frequencies of the different aa or ss
    """
    acids = {}
    if identifier == 'aa':
        acids = {'R': 0.0, 'H': 0.0, 'K': 0.0, 'D': 0.0, 'E': 0.0, 
                 'S': 0.0, 'T': 0.0, 'N': 0.0, 'Q': 0.0, 'C': 0.0,
                 'G': 0.0, 'P': 0.0, 'A': 0.0, 'V': 0.0, 'I': 0.0, 
                 'L': 0.0, 'M': 0.0, 'F': 0.0, 'Y': 0.0, 'W': 0.0}
    else:
        acids = {'C': 0.0, 'E': 0.0, 'H': 0.0}
    if len(sequence) <= 0:
        return [0.0 for key in sorted(acids)]
    for acid in sequence:
        if acid in acids.keys():
            acids[acid] += 1
    return [acids[key]/len(sequence) for key in sorted(acids)]

def get_interface_sequence(interface_location):
    sequence = ''
    interacting_residues = []
    with open(interface_location, 'r') as f:
        for line in f.readlines():
            if line[17:20] in three_to_one.keys():
                sequence += three_to_one[line[17:20]]
            else:
                sequence += 'X'
            interacting_residues.append(int(line[22:26].split()[0]))
    return sequence, interacting_residues

def get_interface_coordinates(interface_location):
    coordinates = []
    with open(interface_location, 'r') as f:
        for line in f.readlines():
            coordinates.append([float(i) for i in line[30:54].split()])
    return coordinates

def calculate_interface_width(coordinates):
    new_coordinates = np.asarray(coordinates).T
    #Calculate the best plane fit to the coordinates
    center = new_coordinates.mean(axis = 1)
    X = new_coordinates - center[:, np.newaxis]
    M = np.dot(X, X.T)
    normal_to_plane = np.linalg.svd(M)[0][:,-1]
    #Project the coordinates onto the plane
    X = X.T
    X = np.asarray([x - np.dot(x, normal_to_plane) * normal_to_plane for x in X])
    #Convert to 2-dimensional coordinates
    x /= math.sqrt((X[0]**2).sum())
    y = np.cross(normal_to_plane, x)
    X = np.asarray([[np.dot(i, x), np.dot(i, y)] for i in X])
    #Convert to polar coordinates
    X = np.asarray([[np.linalg.norm(i), np.arccos(np.clip(np.dot((i/np.linalg.norm(i)), [1.0, 0]), -1.0, 1.0))] for i in X])
    #Search through plane "slices" to find the slice with lowest width
    return 0 #Deprecated solution, we use "fatness" instead

def get_interface_dimensions(file_to_run, SCRIPTS):
    #The fatness script fits an elipsoid to the coordinates.
    #The elipsoid is fitted on the requirement that it should have the same moment of inertia as
    #the point cloud it represents.
    command = SCRIPTS + '/' + 'fatness' + ' ' + file_to_run
    results = os.popen(command).read()
    return sorted([float(i) for i in re.findall('\n[xyz]: (\d+\.\d+)', results)])

def sec_struct_pred(peptide, TMP, PSIPRED, UNIREF):
    peptide_base = os.path.basename(peptide)
    command = 'cp ' + peptide + ' ' + TMP + '/' + peptide_base
    os.system(command)
    command = [PSIPRED + '/' + 'create_profile.sh', TMP + '/' + peptide_base, UNIREF, '6']
    call(command)
    structures = {'C': 0.0, 'E': 0.0, 'H': 0.0}
    command = "awk '{print $3}' " + TMP + '/' + peptide_base.replace('.seq', '.ss2')
    lines = os.popen(command).readlines()
    for line in lines:
        if line[0] in structures:
            structures[line[0]] += 1
    os.system('rm ' + TMP + '/' + peptide_base)
    return [structures[key]/(len(lines)-2) for key in sorted(structures)]

three_to_one = {'ARG': 'R', 'HIS': 'H', 'LYS': 'K', 'ASP': 'D', 'GLU': 'E', 
                'SER': 'S', 'THR': 'T', 'ASN': 'N', 'GLN': 'Q', 'CYS': 'C', 
                'GLY': 'G', 'PRO': 'P', 'ALA': 'A', 'VAL': 'V', 'ILE': 'I', 
                'LEU': 'L', 'MET': 'M', 'PHE': 'F', 'TYR': 'Y', 'TRP': 'W'}

def get_chain_length(chains_file, chain, model):
    command = "sed -nE '/MODEL        " + model + "/,/MODEL/p' " + chains_file + " | grep -e '^.\{13\}CA A.\{4\}" + chain + "' -e '^.\{13\}CA.\{6\}" + chain + "'"
    return len(os.popen(command).readlines())

def mean_sequential_distance(res_num_list):
    seq_dist = 0
    for index, res_num in enumerate(sorted(res_num_list)[:-1]):
        if res_num_list[index + 1] - res_num > 10:
            seq_dist += 10
        else:
            seq_dist += res_num_list[index + 1] - res_num
    return seq_dist/float(len(res_num_list))

class TooLongTimeException(Exception):
    def __init__(self, value):
        self.parameter = value
    def __str__(self):
        return repr(self.parameter)

def signal_handler(signum, frame):
    #raise TooLongTimeException("Stride secondary structure calculator timed out!")
    raise Exception("Stride timed out!")

def tm_signal_handler(signum, frame):
    #raise TooLongTimeException("TMAlign timed out!")
    raise Exception("TMAlign timed out!")

def TMAlign(target, origin_file, model, chain, SCRIPTS, TMP):
    #Fetches the chain the interaction-template comes from
    tma_tmp = tempfile.NamedTemporaryFile(delete = False, dir = TMP)
    tma_tmp.close()
    command = "sed -nE '/MODEL        " + model + "/,/MODEL/p' " + origin_file + " | grep -e '^.\{16\}A.\{4\}" + chain + "' -e '^.\{15\}.\{6\}" + chain + "' > " + tma_tmp.name
    os.system(command)
    #Runs TMAlign between target chain and the origin of the interaction-template
    output = ''
    signal.signal(signal.SIGALRM, tm_signal_handler)
    signal.alarm(10)
    try:
        output = check_output([SCRIPTS + '/' + 'TMAlignC++', target, tma_tmp.name])
    #except TooLongTimeException as e:
    #    pass
    except Exception as e:
        print os.path.basename(origin_file), e
    finally:
        signal.alarm(0)
    #Removes temporary file
    os.system('rm ' + tma_tmp.name)
    #Processing TMAlign output
    TMresults = re.search('Aligned length= +(\d+), RMSD= +([\d\.]+), Seq_ID=.+(\d\.[\d\.]+)\nTM-score= +([\d\.]+).+\nTM-score= +([\d\.]+).+\n\n([\w-]+)\n([ \.:]+)\n([\w-]+)', output)
    output = {}
    #Getting simple features
    try:
        output['TM_aligned_length'] = int(TMresults.group(1))
        output['TM_RMSD'] = float(TMresults.group(2))
        output['TM_sequence_identity'] = float(TMresults.group(3))
        output['TMscore_target'] = float(TMresults.group(4))
        output['TMscore_template'] = float(TMresults.group(5))
    except:
        print("TMalign failed: {}".format(os.path.basename(origin_file)))
        return {'TM_aligned_length': np.nan, 'TM_RMSD': np.nan, 'TM_sequence_identity': np.nan, 'TMscore_target': np.nan, 'TMscore_template': np.nan}, [], []
    #Marking which residues are aligned from the target and which are aligned from the template
    aligned_residues = []
    template_aligned_residues = []
    res_nr = 0
    template_res_nr = 0
    for res, aln, res2 in zip(TMresults.group(6), TMresults.group(7), TMresults.group(8)):
        if aln is ':':
            aligned_residues.append(res_nr)
            template_aligned_residues.append(template_res_nr)
        if not res is '-':
            res_nr += 1
        if not template_res_nr is '-':
            template_res_nr += 1
    #Returns the finished features (as a dictionary) and which residues were aligned (as 2 lists)
    return output, aligned_residues, template_aligned_residues

def peptide_template_ss_comp(origin_file, list_of_interesting_residues, model, chain, SCRIPTS, TMP):
    ss_comp_temp = tempfile.NamedTemporaryFile(delete = False, dir=TMP)
    ss_comp_temp.close()
    command = "sed -nE '/MODEL        " + model + "/,/MODEL/p' " + origin_file + " | grep -e '^.\{16\}A.\{4\}" + chain + "' -e '^.\{15\}.\{6\}" + chain + "' > " + ss_comp_temp.name
    os.system(command)
    binary_list_of_interesting = []
    with open(ss_comp_temp.name, 'r') as f:
        for line in f.readlines():
            if line[:4] == 'ATOM' and line[13:15] == 'CA' and line[16] in 'A1 ':
                if int(line[22:26].split()[0]) in list_of_interesting_residues:
                    binary_list_of_interesting.append(1)
                else:
                    binary_list_of_interesting.append(0)
    stride_output = []
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(10)
    try:
        command = SCRIPTS + '/' + 'stride ' + ss_comp_temp.name + ' | grep "^ASG"'
        stride_output = os.popen(command).readlines()
    #except TooLongTimeException as e:
    #    pass
    except Exception as e:
        raise e
    finally:
        os.system('rm ' + ss_comp_temp.name)
    if len(stride_output) <= 1:
        return [np.nan, np.nan, np.nan]
    converted_structs = {'H': 'H', 'C': 'C', 'T': 'C', 'E': 'E', 
                         'G': 'H', 'B': 'E', 'I': 'H', 'S': 'C'}
    ss_struct_dict = {'C': 0, 'E': 0, 'H': 0}
    try:
        stride_output = [line[24] for index, line in enumerate(stride_output) if binary_list_of_interesting[index]]
    except Exception as e:
        raise e
    for residue in stride_output:
        if residue in converted_structs:
            ss_struct_dict[converted_structs[residue]] += 1
    return [ss_struct_dict[key]/(float(len(stride_output))) for key in sorted(ss_struct_dict)]

def calculate_complimentarity(vector1, vector2):
    vector3 = np.dot(vector1, vector2)/(np.linalg.norm(vector1)*np.linalg.norm(vector2))
    if vector3 <= -1.0:
        return math.pi
    elif vector3 >= 1.0:
        return 0.0
    else:
        return np.arccos(vector3)

def relative_entropies_from_msa(msa_file):
    aa_dict = {'A': 0, 'C': 0, 'E': 0, 'D': 0, 'G': 0, 'F': 0, 'I': 0,
               'H': 0, 'K': 0, 'J': 0, 'M': 0, 'L': 0, 'N': 0, 'Q': 0,
               'P': 0, 'S': 0, 'R': 0, 'T': 0, 'W': 0, 'V': 0, 'Y': 0}
    def occurancy_probabilities(sequence):
        occur_dict = aa_dict.copy()
        for key in occur_dict:
            occur_dict[key] = (len(sequence) - len(sequence.replace(key, '')))/float(len(sequence))
        return occur_dict
    msa = []
    with open(msa_file, 'r') as msa_file_content:
        for line in msa_file_content:
            msa.append(line.split()[1])
    background_probabilities = occurancy_probabilities(''.join(msa))
    relative_entropies = []
    for position, residue in enumerate(msa[0]):
        RE = 0
        probabilities = occurancy_probabilities(''.join([seq[position] for seq in msa]))
        for aa in probabilities.keys():
            if not probabilities[aa] <= 0.0:
                RE += probabilities[aa]*math.log(probabilities[aa]/background_probabilities[aa], 2)
        relative_entropies.append(RE)
    return relative_entropies

BLOSUM62 = {'A': [4, -1, -2, -2, 0, -1, -1, 0, -2, -1, -1, -1, -1, -2, -1, 1, 0, -3, -2, 0, -2, -1, -1, -1], 'C': [0, -3, -3, -3, 9, -3, -4, -3, -3, -1, -1, -3, -1, -2, -3, -1, -1, -2, -2, -1, -3, -1, -3, -1], 'B': [-2, -1, 4, 4, -3, 0, 1, -1, 0, -3, -4, 0, -3, -3, -2, 0, -1, -4, -3, -3, 4, -3, 0, -1], 'E': [-1, 0, 0, 2, -4, 2, 5, -2, 0, -3, -3, 1, -2, -3, -1, 0, -1, -3, -2, -2, 1, -3, 4, -1], 'D': [-2, -2, 1, 6, -3, 0, 2, -1, -1, -3, -4, -1, -3, -3, -1, 0, -1, -4, -3, -3, 4, -3, 1, -1], 'G': [0, -2, 0, -1, -3, -2, -2, 6, -2, -4, -4, -2, -3, -3, -2, 0, -2, -2, -3, -3, -1, -4, -2, -1], 'F': [-2, -3, -3, -3, -2, -3, -3, -3, -1, 0, 0, -3, 0, 6, -4, -2, -2, 1, 3, -1, -3, 0, -3, -1], 'I': [-1, -3, -3, -3, -1, -3, -3, -4, -3, 4, 2, -3, 1, 0, -3, -2, -1, -3, -1, 3, -3, 3, -3, -1], 'H': [-2, 0, 1, -1, -3, 0, 0, -2, 8, -3, -3, -1, -2, -1, -2, -1, -2, -2, 2, -3, 0, -3, 0, -1], 'K': [-1, 2, 0, -1, -3, 1, 1, -2, -1, -3, -2, 5, -1, -3, -1, 0, -1, -3, -2, -2, 0, -3, 1, -1], 'J': [-1, -2, -3, -3, -1, -2, -3, -4, -3, 3, 3, -3, 2, 0, -3, -2, -1, -2, -1, 2, -3, 3, -3, -1], 'M': [-1, -1, -2, -3, -1, 0, -2, -3, -2, 1, 2, -1, 5, 0, -2, -1, -1, -1, -1, 1, -3, 2, -1, -1], 'L': [-1, -2, -3, -4, -1, -2, -3, -4, -3, 2, 4, -2, 2, 0, -3, -2, -1, -2, -1, 1, -4, 3, -3, -1], 'N': [-2, 0, 6, 1, -3, 0, 0, 0, 1, -3, -3, 0, -2, -3, -2, 1, 0, -4, -2, -3, 4, -3, 0, -1], 'Q': [-1, 1, 0, 0, -3, 5, 2, -2, 0, -3, -2, 1, 0, -3, -1, 0, -1, -2, -1, -2, 0, -2, 4, -1], 'P': [-1, -2, -2, -1, -3, -1, -1, -2, -2, -3, -3, -1, -2, -4, 7, -1, -1, -4, -3, -2, -2, -3, -1, -1], 'S': [1, -1, 1, 0, -1, 0, 0, 0, -1, -2, -2, 0, -1, -2, -1, 4, 1, -3, -2, -2, 0, -2, 0, -1], 'R': [-1, 5, 0, -2, -3, 1, 0, -2, 0, -3, -2, 2, -1, -3, -2, -1, -1, -3, -2, -3, -1, -2, 0, -1], 'T': [0, -1, 0, -1, -1, -1, -1, -2, -2, -1, -1, -1, -1, -2, -1, 1, 5, -2, -2, 0, -1, -1, -1, -1], 'W': [-3, -3, -4, -4, -2, -2, -3, -2, -2, -3, -2, -3, -1, 1, -4, -3, -2, 11, 2, -3, -4, -2, -2, -1], 'V': [0, -3, -3, -3, -1, -2, -2, -3, -3, 3, 1, -2, 1, -1, -2, -2, 0, -3, -1, 4, -3, 2, -2, -1], 'Y': [-2, -2, -2, -3, -2, -1, -2, -3, 2, -1, -1, -2, -1, 3, -3, -2, -2, 2, 7, -1, -3, -1, -2, -1], 'X': [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1], 'Z': [-1, 0, 0, 1, -3, 4, 4, -2, 0, -3, -3, 1, -1, -3, -1, 0, -1, -2, -2, -2, 0, -3, 4, -1]}

# --------------------------------- WORKER ---------------------------------11bg2_2B1B
def worker(target, target_structure, peptide_composition_vector, COMPDB, PDB, TMP, SCRIPTS, alignment):
    features_dict = {}
    #Making sure the template still exists in the database
    if not os.path.isfile(COMPDB + '/' + alignment['interface'][:6] + alignment['interface'][8:10] + alignment['interface'][6:8] + alignment['interface'][10:]) or not os.path.isfile(COMPDB + '/' + alignment['interface']):
        return features_dict
    
    #Fetching template origin structure
    origin_structure = TMP + '/' + alignment['interface'] + '_' + str(alignment['index'])
    #command = 'grep -e "^MODEL" -e "^.\{13\}CA.\{6\}' + alignment['interface'][7] + '" -e "^.\{13\}CA.\{6\}' + alignment['interface'][9] + '" ' + PDB + '/' + alignment['interface'][1:3] + '/' +alignment['interface'][:5] + '.pdb >' + origin_structure
    command = 'grep -e "^MODEL" -e "^.\{15\}.\{6\}' + alignment['interface'][7] + '" -e "^.\{15\}.\{6\}' + alignment['interface'][9] + '" ' + PDB + '/' + alignment['interface'][1:3] + '/' +alignment['interface'][:5] + '.pdb >' + origin_structure
    os.system(command)
    
    #Length features
    features_dict['length_template_peptide'] = get_chain_length(origin_structure, alignment['interface'][9], alignment['interface'][8])
    features_dict['length_template_target'] = get_chain_length(origin_structure, alignment['interface'][7], alignment['interface'][6])
    features_dict['interface_size'] = min(alignment['length_smallest'], alignment['length_largest'])
    
    #Getting sequences and sequence dependant features
    peptide_template_sequence, peptide_template_interacting_residues = get_interface_sequence(COMPDB + '/' + alignment['interface'][:6] + alignment['interface'][8:10] + alignment['interface'][6:8] + alignment['interface'][10:])
    #features_dict.update(dict([('aa_comp_pept_template_' + str(i), j) for i, j in enumerate(get_composition(peptide_template_sequence, 'aa'))]))
    features_dict['aa_comp_distance_peptide'] = calculate_complimentarity(peptide_composition_vector, np.average([BLOSUM62[char] for char in peptide_template_sequence], axis = 0))
    suggested_surface_sequence = target.loc[target['res_num'].isin(alignment['target_participants'])]['res_name'].tolist()
    for residue_index in range(len(suggested_surface_sequence)):
        if suggested_surface_sequence[residue_index] in three_to_one.keys():
            suggested_surface_sequence[residue_index] = three_to_one[suggested_surface_sequence[residue_index]]
        else:
            suggested_surface_sequence[residue_index] = 'X'
    suggested_surface_sequence = ''.join(suggested_surface_sequence)
    template_surface_sequence, template_surface_interacting_residues = get_interface_sequence(COMPDB + '/' + alignment['interface'])
    features_dict['aa_comp_distance_target'] = calculate_complimentarity(np.average([BLOSUM62[char] for char in suggested_surface_sequence], axis = 0), np.average([BLOSUM62[char] for char in template_surface_sequence], axis = 0))
    #features_dict.update(dict([('aa_comp_surface_' + str(i), j) for i, j in enumerate(get_composition(suggested_surface_sequence, 'aa'))]))
    
    #Calculating peptide template width
    interface_dimensions = sorted(get_interface_dimensions(COMPDB + '/' + alignment['interface'][:6] + alignment['interface'][8:10] + alignment['interface'][6:8] + alignment['interface'][10:], SCRIPTS))
    if len(interface_dimensions) < 3: #This occurs with perfectly flat or linear interfaces
        interface_dimensions = [0.0 for i in range(3 - len(interface_dimensions))] + interface_dimensions
    features_dict['peptide_template_depth'] = interface_dimensions[0]
    features_dict['peptide_template_width'] = interface_dimensions[1]
    features_dict['peptide_template_length'] = interface_dimensions[2]
    
    #Getting peptide template secondary structure composition
    features_dict.update(dict([('ss_comp_pept_template_' + str(i), j) for i, j in enumerate(peptide_template_ss_comp(origin_structure, peptide_template_interacting_residues, alignment['interface'][8], alignment['interface'][9], SCRIPTS, TMP))]))
    
    #Getting TMAlign-based features
    TMAlign_features, target_TMAligned_residues, template_TMAligned_residues = TMAlign(target_structure, origin_structure, alignment['interface'][6], alignment['interface'][7], SCRIPTS, TMP)
    features_dict.update(TMAlign_features)
    #Looking at overlap between TMAlign and InterComp
    features_dict['target_fraction_IC_prediction_detected_by_TM'] = sum([1 for res in alignment['target_participants'] if res in target_TMAligned_residues])/float(len(alignment['target_participants']))
    features_dict['template_fraction_IC_prediction_detected_by_TM'] = sum([1 for res in alignment['interface_participants'] if res in template_TMAligned_residues])/float(len(alignment['interface_participants']))
    
    #Mean sequential distance
    features_dict['mean_seq_dist_pept_template'] = mean_sequential_distance(alignment['interface_participants'])
    
    #Mean conservation of interaction surface
    interaction_surface = target.loc[target['res_num'].isin(alignment['target_participants'])]
    conservation_coefficients = interaction_surface['conservation'].tolist()
    conservation_coefficients = np.mean(conservation_coefficients)
    rest_of_surface = target.loc[~target['res_num'].isin(alignment['target_participants'])]
    temp = rest_of_surface
    rest_of_surface = rest_of_surface.loc[rest_of_surface['exposure'] >= 0.15]
    conservation_rest_of_surface = rest_of_surface['conservation'].tolist()
    conservation_rest_of_surface = np.mean(conservation_rest_of_surface)
    features_dict['conservation_surface'] = conservation_coefficients/conservation_rest_of_surface
    
    #IS-score for the interaction surface
    RE_interaction_surface = interaction_surface['relative_entropy'].tolist()
    rASA_interaction_surface = interaction_surface['exposure'].tolist()
    RE_rest_of_surface = rest_of_surface['relative_entropy'].tolist()
    rASA_rest_of_surface = rest_of_surface['exposure'].tolist()
    Sscore_interaction = 0
    if not sum(rASA_interaction_surface) == 0:
        Sscore_interaction = sum([i*j for i, j in zip(RE_interaction_surface, rASA_interaction_surface)])/sum(rASA_interaction_surface)
    Sscore_rest = 0
    if not sum(rASA_rest_of_surface) == 0:
        Sscore_rest = sum([i*j for i, j in zip(RE_rest_of_surface, rASA_rest_of_surface)])/sum(rASA_rest_of_surface)
    features_dict['IS-score_surface'] = math.log((1 + Sscore_rest)/(1 + Sscore_interaction))
    
    #Adding simple information from alignment
    features_dict['q-value'] = alignment['q-value']
    features_dict['p-value'] = alignment['p-value']
    if float(alignment['p-value']) > 0.0:
        features_dict['p-value_neglog'] = -math.log(float(alignment['p-value']))
    else:
        features_dict['p-value_neglog'] = 100.0
    features_dict['intercomp_blosum_score'] = alignment['sequence_score']
    features_dict['intercomp_score'] = alignment['best']
    features_dict['1_interface'] = alignment['interface']
    
    #Cleaning
    os.system('rm ' + origin_structure)

    return features_dict

# ---------------------------------- MAIN ----------------------------------
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('target_structure', type = str, help = 'searchpath to pdb-file with target structure')
    parser.add_argument('alignment_pandas', type = str, help = 'searchpath to the .csv-file with the relevant alignments')
    parser.add_argument('peptide_sequence', type = str, help = 'file with peptide sequence (FASTA format)')
    parser.add_argument('-c', '--chunk', type = int, default = 400, help = 'sets how many alignments will be considered between each save of data')
    parser.add_argument('-s', '--softstart', action = 'store_true', default = False, help = 'if you already have some of your results generated and want to pick up where you left off')
    args = parser.parse_args()
    
    #Setting environment variables
    PSIPRED = os.environ["PSIPRED"]
    UNIREF = os.environ["UNIREF"]
    OUTPUT = os.environ["OUTPUT"]
    TMP = os.environ["TMP"]
    PDB = os.environ["PDBBIO"]
    COMPDB = os.environ["COMPDB"]
    SCRIPTS = os.environ["SCRIPTS"]
    HHBLITS = os.environ["HHBLITS"]
    UNIPROT = os.environ["UNIPROT"]

    #Making dictionary for constant features
    feature_dict = {'0_target': os.path.basename(args.target_structure)}
    
    #Establishing the amount of resources available
    cpus = int(os.environ["CPUS"])
    
    #Finding peptide aa composition
    f = open(args.peptide_sequence, 'r')
    lines = f.readlines()[1:]
    peptide_sequence = ''.join(lines).strip()
    #feature_dict.update(dict([('aa_comp_peptide_' + str(i), j) for i, j in enumerate(get_composition(lines[0], 'aa'))]))
    peptide_composition_vector = np.average([BLOSUM62[char] for char in peptide_sequence], axis = 0)
    feature_dict['length_peptide'] = len(peptide_sequence)
    f.close()
    
    #Creating pandas for target residue traits and collecting protein sequence
    f = open(args.target_structure, 'r')
    target = {'res_num': [], 'res_name': []}
    target_sequence = ''
    for line in f.readlines():
        if line[:4] == 'ATOM' and line[13:15] == 'CA' and line[16] in ' 1A':
            target['res_num'].append(int(line[22:26].split()[0]))
            target['res_name'].append(line[17:20].split()[0])
            if line[17:20] in three_to_one.keys():
                target_sequence += three_to_one[line[17:20]]
            else:
                target_sequence += 'X'
    target = pd.DataFrame.from_dict(target)
    f.close()
    feature_dict['length_target'] = len(target_sequence)
    
    #Getting target residue-exposures
    naccess_file = OUTPUT + '/' + os.path.basename(args.target_structure).replace('.pdb', '.rsa')
    if not os.path.isfile(naccess_file):
        os.system('cp ' + args.target_structure + ' ' + TMP)
        os.system(SCRIPTS + '/' + 'naccess ' + TMP + '/' + os.path.basename(args.target_structure))
        os.system('rm ' + os.path.basename(args.target_structure).replace('.pdb', '.log'))
        os.system('rm ' + os.path.basename(args.target_structure).replace('.pdb', '.asa'))
        os.system('mv ' + os.path.basename(args.target_structure).replace('.pdb', '.rsa') + ' ' + OUTPUT)
    with open(naccess_file, 'r') as naccess_profile:
        naccess_results = []
        for line in naccess_profile:
            if line[:3] == 'RES':
                naccess_results.append(float(line[22:28].split()[0])/100.0)
        target = target.assign(exposure = pd.Series(naccess_results))
    
    #Getting target conservation per residue through PSI-BLAST
    target_fasta_file_name = TMP + '/' + os.path.basename(args.target_structure).replace('.pdb', '.seq')
    with open(target_fasta_file_name, 'w') as f:
        f.write('> target\n' + target_sequence)
    target_psi_results = TMP + '/' + os.path.basename(args.target_structure).replace('.pdb', '.psi')
    target_psi_msa = TMP + '/' + os.path.basename(args.target_structure).replace('.pdb', '.aln')
    #"""
    command = PSIPRED + '/../apps/blast-2.2.18_x86_64/bin/blastpgp -a ' + str(cpus) + ' -j 3 -h 0.001 -d ' + UNIREF + ' -F F -i ' + target_fasta_file_name + ' -Q ' + target_psi_results + ' -o ' + target_psi_msa
    print command
    os.system(command)
    #"""
    target = target.assign(conservation = pd.Series([float(i) for i in os.popen("awk '{if(NF>40){print $(NF-1)}}' " + target_psi_results).readlines()]))
    
    #Getting target relative entropy per residue through HHBLITS
    target_msa = TMP + '/' + os.path.basename(args.target_structure).replace('.pdb', '.msa')
    #"""
    command = HHBLITS + '/hhblits -i ' + target_fasta_file_name + ' -id 100 -cov 70 -qid 35 -cpu ' + str(cpus) + ' -d ' + UNIPROT + ' -opsi ' + target_msa
    os.system(command)
    #"""
    #os.system('cp ' + 'output/1abt.pdb__1abtB.seq_featfolder/' + os.path.basename(target_fasta_file_name.replace('.seq', '.msa')) + ' ' + TMP)
    target = target.assign(relative_entropy = pd.Series(relative_entropies_from_msa(target_msa)))
    
    #Predicting peptide secondary structure composition
    pept_ss_comp = sec_struct_pred(args.peptide_sequence, TMP, PSIPRED, UNIREF)
    feature_dict.update(dict([('ss_comp_peptide_' + str(i), j) for i, j in enumerate(pept_ss_comp)]))
    
    #Making folder for results
    feature_folder = OUTPUT + '/' + os.path.basename(args.target_structure) + '__' + os.path.basename(args.peptide_sequence) + '_featfolder'
    os.system('mkdir ' + feature_folder)

    #Loading interface alignments
    alignments = pd.read_csv(args.alignment_pandas)
    #Getting rid of tiny interfaces
    alignments = alignments.loc[alignments['length_smallest'] >= 3]
    #Fixing lists to be actual lists
    def make_int_list(row):
        return [int(i) for i in row[2:-2].split("', '")]
    alignments['target_participants'] = alignments['target_participants'].apply(make_int_list)
    alignments['interface_participants'] = alignments['interface_participants'].apply(make_int_list)
    #def make_int_list(row):
    #    return [int(i) for i in row['interface_participants'][2:-2].split("', '")]
    #alignments['interface_participants'] = alignments.apply(make_int_list, axis = 1)
    #Making a list which can be sent to automatic multiprocessing
    alignments = alignments.assign(index = pd.Series(range(len(alignments))))
    alignments = alignments.to_dict('records')
    
    if not args.softstart:
        #Resetting feature-file
        f = open(feature_folder + '/' + os.path.basename(args.target_structure) + '__' + os.path.basename(args.peptide_sequence) + '_features', 'w')
        f.close()
    elif os.path.isfile(feature_folder + '/' + os.path.basename(args.target_structure) + '__' + os.path.basename(args.peptide_sequence) + '_features'):
        #Finding out which alignments to process
        with open(feature_folder + '/' + os.path.basename(args.target_structure) + '__' + os.path.basename(args.peptide_sequence) + '_features', 'r') as f:
            processed_templates = [line.split(', ')[1] for line in f.readlines()[1:]]
            alignments = [a for a in alignments if not a['interface'] in processed_templates]
        
    
    #Starting work on features which can be calculated in parallell
    chunk_size = args.chunk
    current_chunk = 0
    feature_keys = []
    
    while current_chunk < len(alignments):
        print 'Features generated for {}/{} templates'.format(current_chunk, len(alignments))
        
        """
        #One at a time
        features = []
        for alignment in alignments[current_chunk:min(current_chunk + chunk_size, len(alignments))]:
            features.append(worker(target, args.target_structure, peptide_composition_vector, COMPDB, PDB, TMP, SCRIPTS, alignment))
        """
        #Multiprocessing style
        p = multiprocessing.Pool(cpus)
        func = partial(worker, target, args.target_structure, peptide_composition_vector, COMPDB, PDB, TMP, SCRIPTS)
        features = p.map(func, alignments[current_chunk:min(current_chunk + chunk_size, len(alignments))])
        p.close()
        p.join()
        #"""
        
        #Discarding empty feature_dicts, these arise from sampling alignments to templates since
        #removed from the database
        features = [feats for feats in features if feats]
        
        #Writing to file
        with open(feature_folder + '/' + os.path.basename(args.target_structure) + '__' + os.path.basename(args.peptide_sequence) + '_features', 'a') as out:
            for feat_nr in range(len(features)):
                features[feat_nr].update(feature_dict)
                if current_chunk == 0 and feat_nr == 0:
                    feature_keys = sorted(features[feat_nr].keys())
                    out.write(', '.join([key for key in feature_keys]) + '\n')
                out.write(', '.join([str(features[feat_nr][key]) for key in feature_keys]) + '\n')
        
        current_chunk += chunk_size
    
    #Cleaning
    os.system('mv ' + target_fasta_file_name + ' ' + feature_folder)
    os.system('mv ' + target_fasta_file_name.replace('.seq', '.psi') + ' ' + feature_folder)
    os.system('mv ' + target_fasta_file_name.replace('.seq', '.aln') + ' ' + feature_folder)
    os.system('mv ' + target_fasta_file_name.replace('.seq', '.hhr') + ' ' + feature_folder)
    os.system('mv ' + target_fasta_file_name.replace('.seq', '.msa') + ' ' + feature_folder)
    peptide_sequence_in_tmp = TMP + '/' + os.path.basename(args.peptide_sequence)
    os.system('mv ' + peptide_sequence_in_tmp.replace('.seq', '.chk') + ' ' + feature_folder)
    os.system('mv ' + peptide_sequence_in_tmp.replace('.seq', '.mtx') + ' ' + feature_folder)
    os.system('mv ' + peptide_sequence_in_tmp.replace('.seq', '.psi') + ' ' + feature_folder)
    os.system('mv ' + peptide_sequence_in_tmp.replace('.seq', '.ss2') + ' ' + feature_folder)
    os.system('mv ' + peptide_sequence_in_tmp.replace('.seq', '.fasta.blastpgp') + ' ' + feature_folder)
    
if __name__ == '__main__':
    main()
