#!/usr/bin/env python
import numpy as np
from sklearn.externals import joblib
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import r2_score
import argparse
import re
import os
import pandas as pd
import sys

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type = str, help = 'the file with all features (no final slash)')
    parser.add_argument('forest', type = str, help = '.pkl file with forest to be used')
    args = parser.parse_args()
    
    print "Reading data..."
    data = pd.read_csv(args.input_file, sep = ', ')
    data.replace('None', np.nan, inplace = True)
    data.fillna(value=-100, inplace = True)
    
    features_to_use = ['clashes', 'aa_comp_distance_target', 'ss_comp_peptide_0', 'rGb', 'fraction_aligned_interface', 'conservation_surface', 'TMscore_template', 'aligned_length', 'alignment_RMSD', 'p-value_neglog_peptide', 'interface_size', 'TMscore_target', 'intercomp_blosum_score_peptide', 'length_template_target', 'length_target', 'alignment_sequence_ID', 'link_density', 'intercomp_score_peptide', 'interface_area', 'aa_comp_distance_peptide', 'IS-score_surface', 'length_template_peptide']
    identifier_col_list = [i for i in list(data) if i in ['0_target', '1_interface', '2_fragment']]
    answer_col_list = [i for i in list(data) if 'FACIT' in i]
    data_col_list = [i for i in list(data) if i in features_to_use]
    #data_col_list = [i for i in list(data) if i not in answer_col_list + identifier_col_list]
    
    X = data.as_matrix(columns = data_col_list)
    
    print "Fetching Forest..."
    regr = joblib.load(args.forest)
    
    print "Applying Forest..."
    predictions = regr.predict(X)
    
    data['PREDICTED_DockQ-score'] = predictions
    
    print "Writing Results..."
    OUTPUT = os.environ["OUTPUT"]
    outfolder = OUTPUT + '/' + os.path.basename(args.input_file).replace('features', '') + '_predictions'
    os.system('mkdir ' + outfolder)
    
    data.to_csv(outfolder + '/' + 'randomforestoutput.csv', index = False)

if __name__ == '__main__':
    main()
