#!/usr/bin/env python

import os
import argparse
import re
from subprocess import call
import tempfile

three_to_one = {'ARG': 'R', 'HIS': 'H', 'LYS': 'K', 'ASP': 'D', 'GLU': 'E', 
                'SER': 'S', 'THR': 'T', 'ASN': 'N', 'GLN': 'Q', 'CYS': 'C', 
                'GLY': 'G', 'PRO': 'P', 'ALA': 'A', 'VAL': 'V', 'ILE': 'I', 
                'LEU': 'L', 'MET': 'M', 'PHE': 'F', 'TYR': 'Y', 'TRP': 'W', 'XXX': 'X', 'MSE': 'M'}

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('peptide_sequence', type = str, help = 'path to the peptide sequence-file (fasta format)')
    args = parser.parse_args()
    
    SCRIPTS = os.environ["SCRIPTS"]
    ROSETTA = os.environ["ROSETTA"]
    OUTPUT = os.environ["OUTPUT"]
    TMP = os.environ["TMP"]
    PDB = os.environ["PDBBIO"]
    PSIPRED = os.environ["PSIPRED"]
    BLAST = os.environ["BLAST"]
    UNIREF = os.environ["UNIREF"]
    
    fragment_folder = OUTPUT + '/' + os.path.basename(args.peptide_sequence) + '_fragfolder/'
    os.system('mkdir ' + fragment_folder)
    
    number_of_fragments = 50
    
    #Getting PSIPRED data
    run_psipred(args.peptide_sequence, fragment_folder, TMP, PSIPRED, UNIREF, BLAST, SCRIPTS)
    peptide_in_temp = TMP + '/' + os.path.basename(args.peptide_sequence)
    os.system('cp ' + args.peptide_sequence + ' ' + peptide_in_temp)
    psi_data_location = fragment_folder
    os.system('cp ' + psi_data_location + '/' + os.path.basename(args.peptide_sequence).replace('.seq', '.ss2') + ' ' + TMP)
    os.system('cp ' + psi_data_location + '/' + os.path.basename(args.peptide_sequence).replace('.seq', '.chk') + ' ' + TMP)
    
    #Getting rid of X in sequences (replacing with A)
    new_sequence = ''
    with open(peptide_in_temp, 'r') as f:
        new_sequence = f.read().replace('X', 'A')
    with open(peptide_in_temp, 'w') as f:
        f.write(new_sequence)
    
    #Converting checkpoint file to format readable by rosetta
    command = [SCRIPTS + '/' + 'convert_checkpoint.pl', TMP + '/' + os.path.basename(args.peptide_sequence).replace('.seq', '.chk'), peptide_in_temp]
    call(command)
    
    #Getting length and sequence of the peptide
    peptide_length = 0
    peptide_sequence = ''
    with open(args.peptide_sequence, 'r') as peptide_sequence_file:
        peptide_sequence = peptide_sequence_file.readlines()[1].split()[0]
        peptide_length = len(peptide_sequence)
    print peptide_sequence, peptide_length
    
    #Running fragment picker
    frag_file_prefix = fragment_folder + 'frags' + os.path.basename(args.peptide_sequence)
    frag_file_name = frag_file_prefix + '.' + str(number_of_fragments) + '.' + str(peptide_length) + 'mers'
    vall_file = ROSETTA + '/tools/fragment_tools/vall.jul19.2011.gz'
    if not os.path.isfile(vall_file):
        vall_file = ROSETTA + '/main/tools/fragment_tools/vall.jul19.2011.gz'
    #command = [ROSETTA + '/main/source/bin/fragment_picker.default.linuxgccrelease', '-in:file:vall=' + ROSETTA + '/tools/fragment_tools/vall.jul19.2011.gz', '-database=' + ROSETTA + '/main/database/', '-in:file:checkpoint=' + peptide_in_temp.replace('.seq', '.chk.converted'), '-frags:ss_pred=' + peptide_in_temp.replace('.seq', '.ss2') + ' psipred', '-frags:frag_sizes ' + str(peptide_length), '-frags:n_candidates ' + str(2000), '-frags:n_frags ' + str(number_of_fragments), '-frags:scoring:config ' + SCRIPTS + '/fragment_scoring_weights.cfg', '-frags:bounded_protocol true', '-out:file:frag_prefix ' + frag_file_prefix]
    command = [ROSETTA + '/main/source/bin/fragment_picker.default.linuxgccrelease', '-in:file:vall=' + vall_file, '-database=' + ROSETTA + '/main/database/', '-in:file:checkpoint=' + peptide_in_temp.replace('.seq', '.chk.converted'), '-frags:ss_pred=' + peptide_in_temp.replace('.seq', '.ss2') + ' psipred', '-frags:frag_sizes ' + str(peptide_length), '-frags:n_candidates ' + str(2000), '-frags:n_frags ' + str(number_of_fragments), '-frags:scoring:config ' + SCRIPTS + '/fragment_scoring_weights.cfg', '-frags:bounded_protocol true', '-out:file:frag_prefix ' + frag_file_prefix]
    print command
    os.system(' '.join(command))
    
    #Extracting fragment file information
    frag_lines = []
    with open(frag_file_name, 'r') as frag_file:
        frag_lines = [line for line in frag_file.readlines() if line[0] == ' ']    
    #Iterating over all fragments (which are $peptide_length long) and finding their origin from their PDB files
    #Since fragment files use pdb_seqres numbering (which is different to PDB-numbering), we simply look for the exact sequential motif and extract those residues
    temporary_template_file = tempfile.NamedTemporaryFile(dir = TMP, delete = False)
    temporary_template_file.close()
    temporary_template_file = temporary_template_file.name
    for fragment_nr in range(len(frag_lines)/peptide_length):
        fragment_file_name = TMP + '/' + os.path.basename(args.peptide_sequence).replace('.seq', '') + '_fragment' + str(fragment_nr) + '.pdb'
        res_file_name = fragment_file_name + '_resfile'
        try:
            fragment_sequence = [line.split()[3] for line in frag_lines[fragment_nr*peptide_length:(fragment_nr+1)*peptide_length]]
            fragment_chain = frag_lines[fragment_nr*peptide_length].split()[1]
            template_file_name = frag_lines[fragment_nr*peptide_length].split()[0] + '.pdb'
            template_file_name = PDB + '/' + template_file_name[1:3] + '/' + template_file_name[:4] + '*.pdb'
            os.system('grep "^.\{21\}' + fragment_chain + '" ' + template_file_name + ' > ' + temporary_template_file) 
            lines = []
            with open(temporary_template_file, 'r') as template_file:
                lines = [line.split(':')[-1] for line in template_file.readlines()]
                lines = [line for line in lines if line[:4] in ['ATOM', 'HETA'] and line[16] in 'A 1' and line[21] == fragment_chain] + [''.join(['X' for i in range(80)])]
            template_sequence_number = [(line[17:20], line[21:27]) for line in lines if line[12:16] == ' CA ']
            template_sequence_number = [(three_to_one[i[0]], i[1]) if i[0] in three_to_one.keys() else ('X', i[1]) for i in template_sequence_number]

            template_number = [i[1] for i in template_sequence_number]
            template_sequence = [i[0] for i in template_sequence_number]
            #Finding at what positions the fragment is fetched from
            positions = []
            for position in range(len(template_sequence_number) - peptide_length):
                #if template_sequence[position:position+peptide_length] == fragment_sequence:
                if re.search(''.join(template_sequence[position:position+peptide_length]).replace('X', '.'), ''.join(fragment_sequence)):
                    positions = template_number[position:position+peptide_length]
                    break
            #Extracting those residues and saving to fragment file
            fragment_file_name = TMP + '/' + os.path.basename(args.peptide_sequence).replace('.seq', '') + '_fragment' + str(fragment_nr) + '.pdb'
            last_position = ''
            position_nr = 0
            with open(fragment_file_name, 'w') as fragment_file:
                for line in lines:
                    if line[21:27] in positions:
                        if not line[21:27] == last_position:
                            position_nr += 1
                            last_position = line[21:27]
                        fragment_file.write(line[:16] + ' ' + line[17:20] + ' B  ' + ''.join([' ' for i in range(1-position_nr/10)]) + str(position_nr) + ' ' + line[27:])
                    if line[:3] == 'END' or position_nr > peptide_length:
                        break
            #Creating res_file for mutating residues
            res_file_name = fragment_file_name + '_resfile'
            with open(res_file_name, 'w') as res_file:
                res_file.write('NATAA\nEX 1 EX 2\nUSE_INPUT_SC\nstart\n')
                for i in range(len(fragment_sequence)):
                    if not fragment_sequence[i] == peptide_sequence[i] and not peptide_sequence[i] == 'X':
                        res_file.write(str(i + 1) + ' B PIKAA ' + peptide_sequence[i] + '\n')
            #Mutating residues with fixbb
            command = [ROSETTA + '/main/source/bin/fixbb.default.linuxgccrelease', '-database=' + ROSETTA + '/main/database/', '-in:file:s=' + fragment_file_name, '-resfile ' + res_file_name, '-ex1', '-ex2', '-packing:use_input_sc', '-overwrite', '-out:prefix ' + fragment_folder]
            os.system(' '.join(command))
        except Exception as e:
            print 'Error, no Traceback:', e
        finally:
            #Cleaning
            os.system('rm ' + fragment_file_name)
            os.system('rm ' + res_file_name)

    #Cleaning up
    os.system('rm ' + peptide_in_temp)
    #os.system('mv ' + peptide_in_temp.replace('.seq', '.chk') + ' ' + OUTPUT)
    #os.system('mv ' + peptide_in_temp.replace('.seq', '.ss2') + ' ' + OUTPUT)
    os.system('rm ' + peptide_in_temp.replace('.seq', '.chk'))
    os.system('rm ' + peptide_in_temp.replace('.seq', '.ss2'))
    os.system('mv ' + frag_file_name + ' ' + fragment_folder)
    os.system('rm ' + temporary_template_file)

def run_psipred(peptide, outputfolder, TMP, PSIPRED, UNIREF, BLAST, SCRIPTS):
    peptide_base = os.path.basename(peptide)
    peptide_in_temp = TMP + '/' + os.path.basename(peptide)
    command = 'cp ' + peptide + ' ' + peptide_in_temp
    os.system(command)
    #command = [PSIPRED + '/' + 'create_profile.sh', TMP + '/' + peptide_base, UNIREF, '6']
    command = [SCRIPTS + '/' + 'create_profile.sh', TMP + '/' + peptide_base, UNIREF, '6', BLAST + '/', PSIPRED + '/']
    call(command)
    os.system('mv ' + peptide_in_temp.replace('.seq', '.chk') + ' ' + outputfolder)
    os.system('mv ' + peptide_in_temp.replace('.seq', '.ss2') + ' ' + outputfolder)

if __name__ == '__main__':
    main()
