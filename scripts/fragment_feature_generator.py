#!/usr/bin/env python
import argparse
import pandas as pd
import tempfile
import os
from subprocess import call, check_output, Popen, PIPE
import multiprocessing
from functools import partial
import signal
import numpy as np
import math
import sys
import re
from glob import glob
from threading import Timer

three_to_one = {'ARG': 'R', 'HIS': 'H', 'LYS': 'K', 'ASP': 'D', 'GLU': 'E', 
                'SER': 'S', 'THR': 'T', 'ASN': 'N', 'GLN': 'Q', 'CYS': 'C', 
                'GLY': 'G', 'PRO': 'P', 'ALA': 'A', 'VAL': 'V', 'ILE': 'I', 
                'LEU': 'L', 'MET': 'M', 'PHE': 'F', 'TYR': 'Y', 'TRP': 'W'}

ASA_normalisation = {'A': 123.4115, 'C': 147.4431, 'D': 163.7097, 'E': 195.2893, 'F': 203.8527, 
                     'G': 93.5631, 'H': 198.1719, 'I': 179.6625, 'K': 223.2443, 'L': 193.7956, 
                     'M': 217.3316, 'N': 161.8336, 'P': 159.6743, 'Q': 195.3820, 'R': 256.8007, 
                     'S': 135.2496, 'T': 155.6512, 'V': 163.5877, 'W': 252.3393, 'Y': 234.5152}

ASA_renormalisation = {'A': 114.3228, 'C': 109.8027, 'D': 116.8094, 'E': 113.3755, 'F': 102.1920,
                       'G': 116.8079, 'H': 108.3617, 'I': 102.5939, 'K': 111.1719, 'L': 108.4900,
                       'M': 111.9400, 'N': 112.4313, 'P': 117.2955, 'Q': 109.4577, 'R': 107.5560,
                       'S': 116.0941, 'T': 111.7622, 'V': 108.0215, 'W': 101.1948, 'Y': 110.2252}

Log10PropResBur = {'A': [0.1576, -0.0097, -0.0872, -0.1158], 
                   'C': [0.2842, 0.1824, -0.0851, -0.6021], 
                   'D': [-0.2883, -0.1158, 0.0233, 0.1688], 
                   'E': [-0.5952, -0.2111, -0.0334, 0.2271], 
                   'F': [0.2068, 0.1611, -0.0329, -0.3458], 
                   'H': [-0.1051, 0.0711, 0.1069, -0.0057], 
                   'I': [0.2707, 0.0792, -0.0985, -0.3969], 
                   'K': [-0.9830, -0.3958, -0.0706, 0.2767], 
                   'L': [0.2271, 0.0973, -0.0223, -0.3478], 
                   'M': [0.1726, 0.0330, -0.0209, -0.1938], 
                   'N': [-0.3054, -0.0825, 0.0378, 0.1392], 
                   'P': [-0.2027, -0.0655, 0.0245, 0.1096], 
                   'Q': [-0.4237, -0.1385, 0.0422, 0.1735], 
                   'R': [-0.6383, -0.0899, 0.1129, 0.1741], 
                   'S': [-0.0762, -0.0119, 0.0179, 0.0441], 
                   'T': [-0.0501, -0.0110, 0.0492, 0.0158], 
                   'V': [0.2480, 0.0603, -0.0783, -0.3279], 
                   'W': [0.1014, 0.2467, 0.1075, -0.3391], 
                   'Y': [0.0090, 0.2274, 0.1565, -0.2284]} #Use index1 if rASA <= 0.05, index2 if 0.05 < rASA <= 0.15, index3 if 0.15 < rASA <= 0.30, and index4 if rASA >= 0.30

BLOSUM62 = {'A': [4, -1, -2, -2, 0, -1, -1, 0, -2, -1, -1, -1, -1, -2, -1, 1, 0, -3, -2, 0, -2, -1, -1, -1], 'C': [0, -3, -3, -3, 9, -3, -4, -3, -3, -1, -1, -3, -1, -2, -3, -1, -1, -2, -2, -1, -3, -1, -3, -1], 'B': [-2, -1, 4, 4, -3, 0, 1, -1, 0, -3, -4, 0, -3, -3, -2, 0, -1, -4, -3, -3, 4, -3, 0, -1], 'E': [-1, 0, 0, 2, -4, 2, 5, -2, 0, -3, -3, 1, -2, -3, -1, 0, -1, -3, -2, -2, 1, -3, 4, -1], 'D': [-2, -2, 1, 6, -3, 0, 2, -1, -1, -3, -4, -1, -3, -3, -1, 0, -1, -4, -3, -3, 4, -3, 1, -1], 'G': [0, -2, 0, -1, -3, -2, -2, 6, -2, -4, -4, -2, -3, -3, -2, 0, -2, -2, -3, -3, -1, -4, -2, -1], 'F': [-2, -3, -3, -3, -2, -3, -3, -3, -1, 0, 0, -3, 0, 6, -4, -2, -2, 1, 3, -1, -3, 0, -3, -1], 'I': [-1, -3, -3, -3, -1, -3, -3, -4, -3, 4, 2, -3, 1, 0, -3, -2, -1, -3, -1, 3, -3, 3, -3, -1], 'H': [-2, 0, 1, -1, -3, 0, 0, -2, 8, -3, -3, -1, -2, -1, -2, -1, -2, -2, 2, -3, 0, -3, 0, -1], 'K': [-1, 2, 0, -1, -3, 1, 1, -2, -1, -3, -2, 5, -1, -3, -1, 0, -1, -3, -2, -2, 0, -3, 1, -1], 'J': [-1, -2, -3, -3, -1, -2, -3, -4, -3, 3, 3, -3, 2, 0, -3, -2, -1, -2, -1, 2, -3, 3, -3, -1], 'M': [-1, -1, -2, -3, -1, 0, -2, -3, -2, 1, 2, -1, 5, 0, -2, -1, -1, -1, -1, 1, -3, 2, -1, -1], 'L': [-1, -2, -3, -4, -1, -2, -3, -4, -3, 2, 4, -2, 2, 0, -3, -2, -1, -2, -1, 1, -4, 3, -3, -1], 'N': [-2, 0, 6, 1, -3, 0, 0, 0, 1, -3, -3, 0, -2, -3, -2, 1, 0, -4, -2, -3, 4, -3, 0, -1], 'Q': [-1, 1, 0, 0, -3, 5, 2, -2, 0, -3, -2, 1, 0, -3, -1, 0, -1, -2, -1, -2, 0, -2, 4, -1], 'P': [-1, -2, -2, -1, -3, -1, -1, -2, -2, -3, -3, -1, -2, -4, 7, -1, -1, -4, -3, -2, -2, -3, -1, -1], 'S': [1, -1, 1, 0, -1, 0, 0, 0, -1, -2, -2, 0, -1, -2, -1, 4, 1, -3, -2, -2, 0, -2, 0, -1], 'R': [-1, 5, 0, -2, -3, 1, 0, -2, 0, -3, -2, 2, -1, -3, -2, -1, -1, -3, -2, -3, -1, -2, 0, -1], 'T': [0, -1, 0, -1, -1, -1, -1, -2, -2, -1, -1, -1, -1, -2, -1, 1, 5, -2, -2, 0, -1, -1, -1, -1], 'W': [-3, -3, -4, -4, -2, -2, -3, -2, -2, -3, -2, -3, -1, 1, -4, -3, -2, 11, 2, -3, -4, -2, -2, -1], 'V': [0, -3, -3, -3, -1, -2, -2, -3, -3, 3, 1, -2, 1, -1, -2, -2, 0, -3, -1, 4, -3, 2, -2, -1], 'Y': [-2, -2, -2, -3, -2, -1, -2, -3, 2, -1, -1, -2, -1, 3, -3, -2, -2, 2, 7, -1, -3, -1, -2, -1], 'X': [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1], 'Z': [-1, 0, 0, 1, -3, 4, 4, -2, 0, -3, -3, 1, -1, -3, -1, 0, -1, -2, -2, -2, 0, -3, 4, -1]}

residue_residue_contact_preferences = {'I': {'I': 3.89, 'V': 4.91, 'L': 4.59, 'F': 5.33, 'C': 1.76, 'M': 5.25, 'A': 2.84, 'G': 0.77, 'T': 3.05, 'S': 1.00, 'W': 6.24, 'Y': 5.61, 'P': 3.27, 'H': 3.38, 'E': 3.20, 'Q': 3.60, 'D': 2.30, 'N': 1.59, 'K': 3.23, 'R': 3.80}, 
                                       'V': {'I': 4.91, 'V': 3.74, 'L': 4.20, 'F': 4.69, 'C': 2.89, 'M': 4.37, 'A': 2.57, 'G': -0.41, 'T': 2.83, 'S': 1.42, 'W': 2.92, 'Y': 3.95, 'P': 2.90, 'H': 3.21, 'E': 3.22, 'Q': 3.22, 'D': 1.93, 'N': 1.36, 'K': 4.45, 'R': 4.18}, 
                                       'L': {'I': 4.59, 'V': 4.20, 'L': 4.03, 'F': 4.86, 'C': 2.93, 'M': 5.32, 'A': 2.77, 'G': -0.37, 'T': 2.07, 'S': 1.41, 'W': 5.77, 'Y': 4.19, 'P': 2.50, 'H': 4.88, 'E': 3.12, 'Q': 3.46, 'D': 1.40, 'N': 2.31, 'K': 3.15, 'R': 4.99}, 
                                       'F': {'I': 5.33, 'V': 4.69, 'L': 4.86, 'F': 5.34, 'C': 3.68, 'M': 5.28, 'A': 3.00, 'G': 0.14, 'T': 3.34, 'S': 1.75, 'W': 5.83, 'Y': 5.83, 'P': 4.25, 'H': 3.47, 'E': 2.87, 'Q': 4.25, 'D': 0.99, 'N': 3.11, 'K': 3.57, 'R': 4.49}, 
                                       'C': {'I': 1.76, 'V': 2.89, 'L': 2.93, 'F': 3.68, 'C': 7.65, 'M': 1.84, 'A': 1.46, 'G': -0.25, 'T': 1.03, 'S': 2.48, 'W': 2.14, 'Y': 2.47, 'P': 2.74, 'H': 4.12, 'E': 2.51, 'Q': 1.33, 'D': 0.24, 'N': -0.42, 'K': 2.05, 'R': 2.81}, 
                                       'M': {'I': 5.25, 'V': 4.37, 'L': 5.32, 'F': 5.28, 'C': 1.84, 'M': 6.02, 'A': 2.30, 'G': 0.91, 'T': 2.09, 'S': 1.61, 'W': 4.89, 'Y': 4.81, 'P': 3.38, 'H': 4.65, 'E': 3.88, 'Q': 4.18, 'D': 0.36, 'N': 2.30, 'K': 3.93, 'R': 3.62}, 
                                       'A': {'I': 2.84, 'V': 2.57, 'L': 2.77, 'F': 3.00, 'C': 1.46, 'M': 2.30, 'A': -0.52, 'G': -1.77, 'T': 1.21, 'S': 0.39, 'W': 3.37, 'Y': 2.47, 'P': 1.22, 'H': 2.59, 'E': 1.71, 'Q': 1.72, 'D': 1.13, 'N': 1.69, 'K': 2.13, 'R': 1.90}, 
                                       'G': {'I': 0.77, 'V': -0.41, 'L': -0.37, 'F': 0.14, 'C': -0.25, 'M': 0.91, 'A': -1.77, 'G': -4.40, 'T': 0.21, 'S': -1.53, 'W': 1.42, 'Y': 1.25, 'P': -0.51, 'H': 1.08, 'E': -0.89, 'Q': 0.70, 'D': -0.08, 'N': -0.54, 'K': 1.33, 'R': 1.59}, 
                                       'T': {'I': 3.05, 'V': 2.83, 'L': 2.07, 'F': 3.34, 'C': 1.03, 'M': 2.09, 'A': 1.21, 'G': 0.21, 'T': 1.27, 'S': 1.91, 'W': 5.12, 'Y': 3.14, 'P': 2.65, 'H': 2.71, 'E': 2.88, 'Q': 1.82, 'D': 3.88, 'N': 2.52, 'K': 3.67, 'R': 3.77}, 
                                       'S': {'I': 1.00, 'V': 1.42, 'L': 1.41, 'F': 1.75, 'C': 2.48, 'M': 1.61, 'A': 0.39, 'G': -1.53, 'T': 1.91, 'S': -0.09, 'W': 2.87, 'Y': 2.30, 'P': 1.33, 'H': 0.80, 'E': 2.60, 'Q': 2.00, 'D': 2.94, 'N': 1.77, 'K': 2.74, 'R': 2.82}, 
                                       'W': {'I': 6.24, 'V': 2.92, 'L': 5.77, 'F': 5.83, 'C': 2.14, 'M': 4.89, 'A': 3.37, 'G': 1.42, 'T': 5.12, 'S': 2.87, 'W': 5.85, 'Y': 6.19, 'P': 7.87, 'H': 6.46, 'E': 1.20, 'Q': 1.37, 'D': 2.62, 'N': 3.54, 'K': 5.76, 'R': 8.57}, 
                                       'Y': {'I': 5.61, 'V': 3.95, 'L': 4.19, 'F': 5.83, 'C': 2.47, 'M': 4.81, 'A': 2.47, 'G': 1.25, 'T': 3.14, 'S': 2.30, 'W': 6.19, 'Y': 5.93, 'P': 4.22, 'H': 6.05, 'E': 4.54, 'Q': 2.05, 'D': 1.76, 'N': 3.66, 'K': 5.26, 'R': 5.28}, 
                                       'P': {'I': 3.27, 'V': 2.90, 'L': 2.50, 'F': 4.25, 'C': 2.74, 'M': 3.38, 'A': 1.22, 'G': -0.51, 'T': 2.65, 'S': 1.33, 'W': 7.87, 'Y': 4.22, 'P': 0.60, 'H': 2.89, 'E': 3.17, 'Q': 3.50, 'D': 1.46, 'N': 3.09, 'K': 3.75, 'R': 3.99}, 
                                       'H': {'I': 3.38, 'V': 3.21, 'L': 4.88, 'F': 3.47, 'C': 4.12, 'M': 4.65, 'A': 2.59, 'G': 1.08, 'T': 2.71, 'S': 0.80, 'W': 6.46, 'Y': 6.05, 'P': 2.89, 'H': 5.37, 'E': 2.30, 'Q': 4.00, 'D': 5.20, 'N': 2.38, 'K': 2.72, 'R': 4.90}, 
                                       'E': {'I': 3.20, 'V': 3.22, 'L': 3.12, 'F': 2.87, 'C': 2.51, 'M': 3.88, 'A': 1.71, 'G': -0.89, 'T': 2.88, 'S': 2.60, 'W': 1.20, 'Y': 4.54, 'P': 3.17, 'H': 2.30, 'E': 1.65, 'Q': 1.95, 'D': 0.08, 'N': 2.68, 'K': 5.32, 'R': 5.75}, 
                                       'Q': {'I': 3.60, 'V': 3.22, 'L': 3.46, 'F': 4.25, 'C': 1.33, 'M': 4.18, 'A': 1.72, 'G': 0.70, 'T': 1.82, 'S': 2.00, 'W': 1.37, 'Y': 2.05, 'P': 3.50, 'H': 4.00, 'E': 1.95, 'Q': 2.83, 'D': 3.26, 'N': 3.45, 'K': 3.50, 'R': 4.50}, 
                                       'D': {'I': 2.30, 'V': 1.93, 'L': 1.40, 'F': 0.99, 'C': 0.24, 'M': 0.36, 'A': 1.13, 'G': -0.08, 'T': 3.88, 'S': 2.94, 'W': 2.62, 'Y': 1.76, 'P': 1.46, 'H': 5.20, 'E': 0.08, 'Q': 3.26, 'D': 0.13, 'N': 3.85, 'K': 3.90, 'R': 4.94}, 
                                       'N': {'I': 1.59, 'V': 1.36, 'L': 2.31, 'F': 3.11, 'C': -0.42, 'M': 2.30, 'A': 1.69, 'G': -0.54, 'T': 2.52, 'S': 1.77, 'W': 3.54, 'Y': 3.66, 'P': 3.09, 'H': 2.38, 'E': 2.68, 'Q': 3.45, 'D': 3.85, 'N': 2.92, 'K': 3.17, 'R': 3.85}, 
                                       'K': {'I': 3.23, 'V': 4.45, 'L': 3.15, 'F': 3.57, 'C': 2.05, 'M': 3.93, 'A': 2.13, 'G': 1.33, 'T': 3.67, 'S': 2.74, 'W': 5.76, 'Y': 5.26, 'P': 3.75, 'H': 2.72, 'E': 5.32, 'Q': 3.50, 'D': 3.90, 'N': 3.17, 'K': 3.24, 'R': 2.29}, 
                                       'R': {'I': 3.80, 'V': 4.18, 'L': 4.99, 'F': 4.49, 'C': 2.81, 'M': 3.62, 'A': 1.90, 'G': 1.59, 'T': 3.77, 'S': 2.82, 'W': 8.57, 'Y': 5.28, 'P': 3.99, 'H': 4.90, 'E': 5.75, 'Q': 4.50, 'D': 4.94, 'N': 3.85, 'K': 2.29, 'R': 2.87}}

def get_contact_features(contact_map, pept_residues, sequence):
    clashes = 0
    link_density = 0
    ICP = 0.0
    peptide_residues = pept_residues
    if len(contact_map) > len(sequence):
        #Sequence is based on CA, but contact_map is made with any heavy atoms. This means a contact can be made for a residue without a CA, which would not add to the sequence
        contact_map = [c[:min(peptide_residues)-1] + c[min(peptide_residues):] for c in contact_map]
        contact_map = contact_map[:min(peptide_residues)-1] + contact_map[min(peptide_residues):]
        peptide_residues = [p-1 for p in peptide_residues]
    for pres in peptide_residues:
        for chain_res_index, distance in enumerate(contact_map[pres][:min(peptide_residues)]):
            if distance <= 6.0:
                link_density += 1
                if sequence[pres] in residue_residue_contact_preferences.keys() and sequence[chain_res_index] in residue_residue_contact_preferences.keys():
                    ICP += residue_residue_contact_preferences[sequence[pres]][sequence[chain_res_index]]
                else:
                    ICP += 2.9056 #This is the mean preference, used for X
                if distance <= 2.2: #Clash distance as defined by RCSB (symmetry clashes)
                    clashes += 1
    if not link_density == 0:
        ICP = ICP / link_density
    link_density = link_density / float(len(peptide_residues))
    return link_density, ICP, clashes

class TooLongTimeException(Exception):
    def __init__(self, value):
        self.parameter = value
    def __str__(self):
        return repr(self.parameter)

def signal_handler(signum, frame):
    #raise TooLongTimeException("Contact matrix calculator timed out!")
    raise Exception("Contact matrix calculator timed out!")

def naccess_signal_handler(signum, frame):
    #raise TooLongTimeException("Naccess timed out!")
    raise Exception("Naccess timed out!")

def shape_signal_handler(signum, frame):
    #raise TooLongTimeException("ccp4 timed out!")
    raise Exception("Shape complementarity calculator timed out!")

contact_map_row_pattern = re.compile('(\w):([ \d\.]+)\n')

def get_contact_matrix(structure, SCRIPTS):
    output = ''
    error = ''
    try:
        proc = Popen('exec ' + SCRIPTS + '/' + 'contact_map_chain ' + structure, shell = True, stdout = PIPE, stderr = PIPE)
        timer = Timer(10, proc.kill)
        try:
            timer.start()
            output, error = proc.communicate()
        finally:
            timer.cancel()
        if error:
            raise Exception(error)
    except Exception as e:
        print 'Contact matrix calculator messed up!', e
        return None, None
    output = re.findall(contact_map_row_pattern, output)
    peptide_residues = [i for i, j in enumerate(output) if j[0] == 'B']
    output = [[float(i) for i in o[1].split()] for o in output]
    return output, peptide_residues

def make_aligned_complex(full_target, interface, peptide, SCRIPTS, TMP, COMPDB):
    complement_interface = interface[:6] + interface[8:10] + interface[6:8] + interface[10:]
    #The larger structure is always superpositioned on the smaller one
    aligned_file = full_target
    final_aligned_file = aligned_file
    #Superpositions the peptide on the complementary interface (or vice versa)
    peptide_length = int(os.popen('grep "TER" -B 1 ' + peptide).read()[22:26].split()[0])
    #interface_size = int(os.popen('wc -l ' + COMPDB + '/' + complement_interface).read().split()[0])
    interface_size = int(os.popen('wc -l ' + COMPDB.format(complement_interface, complement_interface[1:3])).read().split()[0])
    peptide_aligned_file = peptide
    alignment_info = ''
    if peptide_length >= interface_size:
        peptide_copy = tempfile.NamedTemporaryFile(delete = False, dir = TMP)
        peptide_copy.close()
        peptide_copy = peptide_copy.name
        os.system('cp ' + peptide + ' ' + peptide_copy)
        #command = '(cd ' + TMP + ' && exec ' + SCRIPTS + '/' + 'InterfaceComparison -PDB ' + COMPDB + '/' + complement_interface + ' ' + peptide + ' -super -apply ' + peptide_copy + ' -map)'
        command = '(cd ' + TMP + ' && exec ' + SCRIPTS + '/' + 'InterfaceComparison -PDB ' + COMPDB.format(complement_interface, complement_interface[1:3]) + ' ' + peptide + ' -super -apply ' + peptide_copy + ' -map)'
        #alignment_info = os.popen(command).read()
        try:
            alignment_info = check_output(command, shell = True)
        except:
            print 'InterComp failed at {}'.format(interface)
            os.system('rm ' + peptide_copy)
            return None, None
        peptide_aligned_file = TMP + '/' + os.path.basename(peptide_copy) + '_' + os.path.basename(complement_interface)
        os.system('rm ' + peptide_copy)
    else:
        #command = '(cd ' + TMP + ' && exec ' + SCRIPTS + '/' + 'InterfaceComparison -PDB ' + COMPDB + '/' + complement_interface + ' ' + peptide + ' -super -apply ' + aligned_file + ' -map)'
        command = '(cd ' + TMP + ' && exec ' + SCRIPTS + '/' + 'InterfaceComparison -PDB ' + COMPDB.format(complement_interface, complement_interface[1:3]) + ' ' + peptide + ' -super -apply ' + aligned_file + ' -map)'
        #alignment_info = os.popen(command).read()
        try:
            alignment_info = check_output(command, shell = True)
        except:
            print 'InterComp failed at {}'.format(interface)
            return None, None
        final_aligned_file = TMP + '/' + os.path.basename(aligned_file) + '_' + os.path.basename(peptide)

    #Splices the two aligned files
    spliced_file = tempfile.NamedTemporaryFile(delete=False, dir=TMP)
    spliced_file.close()
    if alignment_info:
        os.system('grep "ATOM" ' + final_aligned_file + ' > ' + spliced_file.name)
        os.system('echo "TER\n" >> ' + spliced_file.name)
        os.system('grep "ATOM" ' + peptide_aligned_file + ' >> ' + spliced_file.name)
    
    #Cleaning
    os.system('rm ' + TMP + '/' + os.path.basename(complement_interface) + '_' + os.path.basename(peptide))
    os.system('rm ' + TMP + '/' + os.path.basename(peptide) + '_' + os.path.basename(complement_interface))
    if not final_aligned_file == aligned_file:
        os.system('rm ' + final_aligned_file)
    if not peptide_aligned_file == peptide:
        os.system('rm ' + peptide_aligned_file)
    return alignment_info, spliced_file.name

def run_naccess(structure, SCRIPTS, TMP, NACCESS):
    output = []
    signal.signal(signal.SIGALRM, naccess_signal_handler)
    signal.alarm(10)
    try:
        os.system('cp ' + structure + ' ' + structure + '.pdb')
        os.system('(cd ' + TMP + ' && exec ' + os.path.abspath(SCRIPTS) + '/' + 'naccess ' + os.path.abspath(structure) + '.pdb ' + NACCESS + ')')
        with open(structure + '.rsa', 'r') as f:
            output = f.readlines()
        os.system('rm ' + structure + '.asa')
        os.system('rm ' + structure + '.rsa')
        os.system('rm ' + structure + '.log')
    except TooLongTimeException as e:
        os.system('rm ' + structure + '.pdb')
        return None
    except Exception as e:
        print e
    finally:
        signal.alarm(0)
        os.system('rm ' + structure + '.pdb')
    output = [line for line in output if line[:3] == 'RES']
    #abs_output = [float(line[16:22].split()[0]) for line in output]
    #output = [float(line[22:28].split()[0]) for line in output]
    output = [float(line[16:22].split()[0]) for line in output]
    return output#, abs_output

def calc_rGb(sequence, exposure):
    sequence_no_x = sequence.replace('X', '')
    n_residues = len(sequence_no_x.replace('G', ''))
    rGb = 0
    if n_residues == 0:
        return rGb
    for index, residue in enumerate(sequence_no_x):
        if residue is 'G' or residue is 'X':#or index >= len(exposure):
            continue
        relative_exposure = exposure[index]/ASA_normalisation[residue]
        #relative_exposure = exposure[index]/ASA_renormalisation[residue]
        if relative_exposure > 0.30:
            rGb += Log10PropResBur[residue][3]
        elif relative_exposure > 0.15:
            rGb += Log10PropResBur[residue][2]
        elif relative_exposure > 0.05:
            rGb += Log10PropResBur[residue][1]
        else:
            rGb += Log10PropResBur[residue][0]
    rGb = rGb / n_residues
    return rGb

alignment_pattern = re.compile('Best: ([\d\.]+) sequence score: ([\d-]+).+p-value: ([\d\.e-]+)')

def align_reader(alignment):
    match = re.search(alignment_pattern, alignment)
    try:
        return [float(i) for i in [match.group(1), match.group(2), match.group(3)]]
    except Exception as e:
        print 'alignment exception:', e
        print alignment
        return [0, 0, 1.00]

def run_DockQ(path, compare_file, native_file):
    #Returns DockQ-score, fnat, iRMS, LRMS
    dockq_results = check_output(path + '/DockQ.py ' + compare_file + ' ' + native_file + ' -short', shell = True)
    try:
        return [float(i) for i in [dockq_results.split()[1], dockq_results.split()[3], dockq_results.split()[5], dockq_results.split()[7]]]
    except ValueError as e:
        return []

def superposition(target, template, template_chain, template_model, SCRIPTS, TMP):
    #Superpositions the target with chain template_chain of model template_model of the template
    #(using TMalign)
    super_positioned_target = tempfile.NamedTemporaryFile(delete = False, dir = TMP)
    super_positioned_target.close()
    #Fetching only interesting model
    os.system("sed -n '/MODEL        " + template_model + "/,/MODEL/p' " + template + " > " + super_positioned_target.name)
    temporary_structure = tempfile.NamedTemporaryFile(delete = False, dir = TMP)
    temporary_structure.close()
    #Fetching only interesting chain of interesting model
    os.system('grep "^.\{15\}.\{6\}' + template_chain + '" ' + super_positioned_target.name + ' > ' + temporary_structure.name)
    #Running TMAlign
    command = SCRIPTS + '/' + 'TMAlignC++ -B ' + target + ' -A ' + temporary_structure.name + ' -oa ' +  super_positioned_target.name
    try:
        null = check_output(command, shell=True)
        os.system('rm ' + temporary_structure.name)
        os.system('rm ' + super_positioned_target.name)
        os.system('rm ' + super_positioned_target.name + '_atm')
    except:
        os.system('rm ' + temporary_structure.name)
        os.system('rm ' + super_positioned_target.name)
        os.system('rm ' + super_positioned_target.name + '_atm')
        os.system('rm ' + super_positioned_target.name + '_all_atm')
        return None
    repositioned_target = tempfile.NamedTemporaryFile(delete = False, dir = TMP)
    repositioned_target.close()
    os.system('grep "^.\{15\}.\{6\}A" ' + super_positioned_target.name + '_all_atm > ' + repositioned_target.name)
    os.system('rm ' + super_positioned_target.name + '_all_atm')
    return repositioned_target.name

def get_interface_residues(target, interface, TMP, SCRIPTS):
    temporary_file = tempfile.NamedTemporaryFile(delete = False, dir = TMP)
    temporary_file.close()
    interface_basename = os.path.basename(interface)
    os.system('cat ' + os.path.dirname(interface) + '/' + interface_basename[:6] + interface_basename[8:10] + interface_basename[6:8] + interface_basename[10:] + ' > ' + temporary_file.name)
    complement_interface_size = int(os.popen('wc -l ' + temporary_file.name).read().split()[0])
    os.system('cat ' + target + ' >> ' + temporary_file.name)
    output = os.popen(SCRIPTS + '/' + 'contact_map_chain ' + temporary_file.name).read()
    contacts = re.findall(contact_map_row_pattern, output)
    contacts = [[float(c) for c in cont[1].split()] for cont in contacts]
    residues_in_contact = []
    for residue in contacts[complement_interface_size:]:
        in_contact = False
        for contact in residue[:complement_interface_size]:
            if contact <= 6.0:
                in_contact = True
                break
        residues_in_contact.append(in_contact)
    os.system('rm ' + temporary_file.name)
    return residues_in_contact

# ---------------------------- WORKER - WRAPPER ----------------------------
def workerwrapper(target, target_structure, fragments, complex_sequence, peptide_composition_vector, COMPDB, PDB, TMP, SCRIPTS, NACCESS, DOCKQ, interface_list, native_structure, fragment_profiles, alignment):
    feature_dicts = []
    #Getting potential interfaces for this alignment
    interfaces = os.popen('grep "' + alignment['template'][:4] + '[0-9]_[0-9]' + alignment['template'][4] + '" ' + interface_list).readlines()
    if not interfaces:
        print 'No interfaces for {}!'.format(alignment['template'])
        return feature_dicts
    interfaces = [interface.strip() for interface in interfaces]
    
    #Adding already calculated features
    precalculated_features = {}
    precalculated_features['length_template_target'] = alignment['template_length']
    precalculated_features['aligned_length'] = alignment['aligned_length']
    precalculated_features['alignment_RMSD'] = alignment['alignment_RMSD']
    precalculated_features['alignment_sequence_ID'] = alignment['alignment_sequence_ID']
    precalculated_features['TMscore_target'] = alignment['TMscore_target']
    precalculated_features['TMscore_template'] = alignment['TMscore_template']
    
    for interface in interfaces:
        base_feature_dict = precalculated_features.copy()
        interface_name = os.path.basename(interface)
        base_feature_dict['1_interface'] = interface_name
        
        #Checking if interface is a domain-domain interface rather than chain-chain
        domain_interface = False
        base_feature_dict['domain_interface'] = 0
        if interface_name.split('_')[-1] in ['d', 'd.pdb']:
            domain_interface = True
            base_feature_dict['domain_interface'] = 1
        
        #Fetching template origin structure
        origin_structure = tempfile.NamedTemporaryFile(delete = False, dir = TMP)
        origin_structure.close()
        command = 'grep -e "^MODEL" -e "^.\{15\}.\{6\}' + interface_name[7] + '" -e "^.\{15\}.\{6\}' + interface_name[9] + '" ' + PDB + '/' + interface_name[1:3] + '/' + interface_name[:5] + '.pdb >' + origin_structure.name
        os.system(command)
        
        #Repositioning target over template origin as dictated by TMAlign
        if not domain_interface:
            positioned_target = superposition(target_structure, origin_structure.name, interface_name[7], interface_name[6], SCRIPTS, TMP)
        else:
            positioned_target = superposition(target_structure, origin_structure.name, interface_name[7], '1', SCRIPTS, TMP)
        if not positioned_target:
            continue
        
        #Length features
        if not domain_interface:
            base_feature_dict['length_template_peptide'] = get_chain_length(origin_structure.name, interface_name[9], interface_name[8])
        else:
            base_feature_dict['length_template_peptide'] = get_chain_length(origin_structure.name, interface_name[9], '1')
        interface_size = int(os.popen('wc -l ' + interface).read().split()[0])
        base_feature_dict['interface_size'] = interface_size
        
        #Calculating suggested interface
        interface_residues = get_interface_residues(positioned_target, interface, TMP, SCRIPTS) #This is a binary list with True for in contact and False for not
        if not True in interface_residues:
            print 'No Interface!', interface_name
            continue
        elif len(interface_residues) > len(target):
            interface_residues = interface_residues[:len(target)]

        #Calculating what fraction of the interface is aligned in TMalign
        fraction_aligned_interface = zip(list(alignment['TMALIGNMENT_target']), list(alignment['TMALIGNMENT_key']))
        fraction_aligned_interface = [1.0 if res[1] == ':' else 0.5 if res[1] == '.' else 0.0 for res in fraction_aligned_interface if not res[0] == '-']
        fraction_aligned_interface = [res[0] for res in zip(fraction_aligned_interface, interface_residues) if res[1]]
        if len(fraction_aligned_interface) == 0:
            fraction_aligned_interface = 0
        else:
            fraction_aligned_interface = sum(fraction_aligned_interface)/len(fraction_aligned_interface)
        base_feature_dict['fraction_aligned_interface'] = fraction_aligned_interface
        
        #Getting sequences and sequence dependant features
        #peptide_template_sequence, peptide_template_interacting_residues = get_interface_sequence(COMPDB + '/' + interface_name[:6] + interface_name[8:10] + interface_name[6:8] + interface_name[10:])
        peptide_template_sequence, peptide_template_interacting_residues = get_interface_sequence(COMPDB.format(interface_name[:6] + interface_name[8:10] + interface_name[6:8] + interface_name[10:], interface_name[1:3]))
        base_feature_dict['aa_comp_distance_peptide'] = calculate_complimentarity(peptide_composition_vector, np.average([BLOSUM62[char] for char in peptide_template_sequence], axis = 0))
        
        suggested_surface_sequence = target.loc[interface_residues]['res_name'].tolist()
        for residue_index in range(len(suggested_surface_sequence)):
            if suggested_surface_sequence[residue_index] in three_to_one.keys():
                suggested_surface_sequence[residue_index] = three_to_one[suggested_surface_sequence[residue_index]]
            else:
                suggested_surface_sequence[residue_index] = 'X'
        suggested_surface_sequence = ''.join(suggested_surface_sequence)
        #template_surface_sequence, template_surface_interacting_residues = get_interface_sequence(COMPDB + '/' + interface_name)
        template_surface_sequence, template_surface_interacting_residues = get_interface_sequence(COMPDB.format(interface_name, interface_name[1:3]))
        base_feature_dict['aa_comp_distance_target'] = calculate_complimentarity(np.average([BLOSUM62[char] for char in suggested_surface_sequence], axis = 0), np.average([BLOSUM62[char] for char in template_surface_sequence], axis = 0))
        
        #Mean conservation of interaction surface
        interaction_surface = target.loc[interface_residues]['conservation'].tolist()
        rest_of_surface = target.loc[[False if i == True else True for i in interface_residues]]
        rest_of_surface = rest_of_surface.loc[rest_of_surface['exposure'] >= 0.15]
        rest_of_surface = rest_of_surface['conservation'].tolist()
        interaction_surface = np.mean(interaction_surface)
        rest_of_surface = np.mean(rest_of_surface)
        if rest_of_surface > 0.0:
            base_feature_dict['conservation_surface'] = interaction_surface/rest_of_surface
        else:
            base_feature_dict['conservation_surface'] = None
        
        #IS-score for the interaction surface
        RE_interaction_surface = target.loc[interface_residues]['relative_entropy'].tolist()
        rASA_interaction_surface = target.loc[interface_residues]['exposure'].tolist()
        rest_of_surface = target.loc[[False if i == True else True for i in interface_residues]]
        rest_of_surface = rest_of_surface.loc[rest_of_surface['exposure'] >= 0.15]
        RE_rest_of_surface = rest_of_surface['relative_entropy'].tolist()
        rASA_rest_of_surface = rest_of_surface['exposure'].tolist()
        Sscore_interaction = 0
        if not sum(rASA_interaction_surface) == 0:
            Sscore_interaction = sum([i*j for i, j in zip(RE_interaction_surface, rASA_interaction_surface)])/sum(rASA_interaction_surface)
        Sscore_rest = 0
        if not sum(rASA_rest_of_surface) == 0:
            Sscore_rest = sum([i*j for i, j in zip(RE_rest_of_surface, rASA_rest_of_surface)])/sum(rASA_rest_of_surface)
        base_feature_dict['IS-score_surface'] = math.log((1 + Sscore_rest)/(1 + Sscore_interaction))
        
        os.system('rm ' + origin_structure.name)
        
        print '----------------START----------------', base_feature_dict['1_interface']
        sys.stdout.flush()
        
        for fragment in fragments:
            feature_dicts.append(worker(target, target_structure, fragment, complex_sequence, COMPDB, PDB, TMP, SCRIPTS, NACCESS, DOCKQ, native_structure, alignment, interface, base_feature_dict, fragment_profiles, positioned_target))
            
        print '-----------------END-----------------', base_feature_dict['1_interface']
        sys.stdout.flush()
            
        os.system('rm ' + positioned_target)
    
    return feature_dicts

# --------------------------------- WORKER ---------------------------------11bg2_2B1B
def worker(target, target_structure, fragment, complex_sequence, COMPDB, PDB, TMP, SCRIPTS, NACCESS, DOCKQ, native_structure, alignment, interface, base_features_dict, fragment_profiles, aligned_target):
    features_dict = base_features_dict.copy()
    features_dict['2_fragment'] = os.path.basename(fragment)
    
    #Superpositioning target and fragment to their interfaces
    #alignment_output, interaction_complex = runInterComp(os.path.abspath(target_structure), os.path.abspath(target_shell_structure), alignment['1_interface'], os.path.abspath(fragment), SCRIPTS, TMP, COMPDB)
    alignment_output, interaction_complex = make_aligned_complex(os.path.abspath(aligned_target), base_features_dict['1_interface'], os.path.abspath(fragment), SCRIPTS, TMP, COMPDB)
    if not interaction_complex:
        return {}
    
    #Fetching peptide alignment data
    processed_alignment_output = align_reader(alignment_output)
    #Returning empty dictionary if the p-value is too terrible
    #if float(processed_alignment_output[2]) >= 0.3:
    #    os.system('rm ' + interaction_complex)
    #    return {}
    features_dict['intercomp_score_peptide'] = processed_alignment_output[0]
    features_dict['intercomp_blosum_score_peptide'] = processed_alignment_output[1]
    #features_dict['p-value_peptide'] = processed_alignment_output[2]
    pvalue = processed_alignment_output[2]
    #if float(features_dict['p-value_peptide']) > 0.0:
    if pvalue > 0.0:
        #features_dict['p-value_neglog_peptide'] = -math.log(float(features_dict['p-value_peptide']))
        features_dict['p-value_neglog_peptide'] = -math.log(float(pvalue))
    else:
        features_dict['p-value_neglog_peptide'] = 100.0

    #Fetching contact matrix
    contact_matrix, peptide_residues = get_contact_matrix(interaction_complex, SCRIPTS)
    
    #Getting link density, interface contact preferance, and number of clashes
    if contact_matrix:
        link_density, interface_contact_preferance, n_clashes = get_contact_features(contact_matrix, peptide_residues, complex_sequence)
        features_dict['link_density'] = link_density
        #features_dict['interface_contact_preferance'] = interface_contact_preferance
        features_dict['clashes'] = n_clashes
    else:
        features_dict['link_density'] = np.nan
        #features_dict['interface_contact_preferance'] = np.nan
        features_dict['clashes'] = np.nan
    
    #Calculating exposure profile
    exposure_profile = run_naccess(interaction_complex, SCRIPTS, TMP, NACCESS)
    if not exposure_profile:
        return {}
    
    features_dict['rGb'] = calc_rGb(complex_sequence, exposure_profile)

    #features_dict['interface_area'] = sum(target['abs_exposure'].tolist()) - sum(exposure_profile[:len(target)])
    features_dict['interface_area'] = (sum(target['abs_exposure'].tolist()) + sum(fragment_profiles[features_dict['2_fragment']]) - sum(exposure_profile))/2.0
    
    #If there is a native structure, runs DockQ
    if native_structure:
        facit_scores = run_DockQ(DOCKQ, interaction_complex, native_structure)
        if not facit_scores:
            os.system('rm ' + interaction_complex)
            return {}
        features_dict['FACIT_DockQ-score'] = facit_scores[0]
        features_dict['FACIT_Fnat'] = facit_scores[1] 
        features_dict['FACIT_iRMS'] = facit_scores[2]
        features_dict['FACIT_LRMS'] = facit_scores[3]

    #Cleaning up
    os.system('rm ' + interaction_complex)
    
    return features_dict

# ---------------------------------- MAIN ----------------------------------
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('target_structure', type = str, help = 'searchpath to pdb-file with target structure')
    parser.add_argument('alignment_pandas', type = str, help = 'searchpath to the csv file with alignment data')
    parser.add_argument('peptide_sequence', type = str, help = 'file with peptide sequence (FASTA format)')
    parser.add_argument('-c', '--chunk', type = int, default = 160, help = 'sets how many alignments will be considered between each save of data')
    parser.add_argument('-s', '--softstart', action = 'store_true', default = False, help = 'if you already have some of your results generated and want to pick up where you left off')
    parser.add_argument('-n', '--native', type = str, help = 'path to native file for the complex. If this is set, DockQ-metrics are calculated')
    parser.add_argument('-o', '--output_suffix', type = str, default = '_interpepfeatures', help = 'suffix for output')
    parser.add_argument('-d', '--interfaces_list', type = str, default = 'interfaces_full_list', help = 'name of file with paths to interfaces')
    parser.add_argument('-e', '--early_stop', action = 'store_true', help = 'Use if you only want to run one chunk (good for testing if stuff works)')
    parser.add_argument('-O', '--other', action = 'store_true', help = 'set to True if there are any strange PTM in the peptide sequence')
    parser.add_argument('-P', '--phosphorylated', action = 'store_true', help = 'set to True if there are any phosphorylated amino acids in the peptide sequence')
    parser.add_argument('-M', '--methylated', action = 'store_true', help = 'set to True if there are any methylated amino acids in the peptide sequence')
    args = parser.parse_args()
    
    #Setting environment variables
    PSIPRED = os.environ["PSIPRED"]
    BLAST = os.environ["BLAST"]
    OUTPUT = os.environ["OUTPUT"]
    TMP = os.environ["TMP"]
    PDB = os.environ["PDBBIO"]
    COMPDB = os.environ["COMPDB"]
    SCRIPTS = os.environ["SCRIPTS"]
    DOCKQ = os.environ["DOCKQ"]
    NACCESS = os.environ["NACCESS"]
    UNIREF = os.environ["UNIREF"]
    UNIPROT = os.environ["UNIPROT"]
    HHBLITS = os.environ["HHBLITS"]
    
    #Getting list of interfaces
    DB = os.environ["DB"]
    #interface_list = DB + '/interfaces_reduced_list'
    #interface_list = DB + '/interfaces_full_list'
    interface_list = DB + '/' + args.interfaces_list

    #Formatting COMPDB string
    with open(interface_list, 'r') as f:
        test_interface = f.readline().split('/')
        if test_interface[-2] == test_interface[-1][1:3]:
            COMPDB = COMPDB + '/{1}/{0}'
        else:
            COMPDB = COMPDB + '/{0}'

    #Making dictionary for constant features
    feature_dict = {'0_target': os.path.basename(args.target_structure)}

    #Establishing the amount of resources available
    cpus = int(os.environ["CPUS"])
    
    #Finding peptide sequence
    f = open(args.peptide_sequence, 'r')
    lines = f.readlines()[1:]
    peptide_sequence = ''.join(lines).strip()
    f.close()
    peptide_composition_vector = np.average([BLOSUM62[char] for char in peptide_sequence], axis = 0)
    
    #Getting list of available fragments
    fragments = sorted(glob(OUTPUT + '/' + os.path.basename(args.peptide_sequence) + '_fragfolder/' + '*.pdb'))
    #Sanity-checking said list and excluding strange fragments
    old_fragments = fragments[:]
    fragments = []
    for fragment in old_fragments:
        with open(fragment, 'r') as f:
            residues = f.readlines()
            residues = [line for line in residues if line[:4] == 'ATOM']
            residues = [line for line in residues if line[13:15] == 'CA']
            if len(residues) == len(peptide_sequence):
                fragments.append(fragment)

    #Making exposure profiles for fragments
    fragment_profiles = {}
    tmpfragment = tempfile.NamedTemporaryFile(dir = TMP, delete = False)
    tmpfragment.close()
    tmpfragment = tmpfragment.name
    for fragment in fragments:
        os.system('cp ' + fragment + ' ' + tmpfragment)
        fragment_profiles[os.path.basename(fragment)] = run_naccess(tmpfragment, SCRIPTS, TMP, NACCESS)
    os.system('rm ' + tmpfragment)

    #Creating copy of target without ambiguous stuff
    target_in_tmp = TMP + '/' + os.path.basename(args.target_structure)
    with open(args.target_structure, 'r') as origin:
        with open(target_in_tmp, 'w') as new_struct:
            for line in origin:
                if line[:4] == 'ATOM':
                    if line[16] in ' 1A':
                        new_struct.write(line)
                else:
                    new_struct.write(line)
    args.target_structure = target_in_tmp
    
    #Creating pandas for target residue traits and collecting protein sequence
    f = open(args.target_structure, 'r')
    target = {'res_num': [], 'res_name': []}
    target_sequence = ''
    for line in f.readlines():
        if line[:4] == 'ATOM' and line[13:15] == 'CA' and line[16] in ' 1A':
            target['res_num'].append(int(line[22:26].split()[0]))
            target['res_name'].append(line[17:20].split()[0])
            if line[17:20] in three_to_one.keys():
                target_sequence += three_to_one[line[17:20]]
            else:
                target_sequence += 'X'
    target = pd.DataFrame.from_dict(target)
    f.close()
    feature_dict['length_target'] = len(target_sequence)
    
    #All suggested interaction-complexes will have the same sequence
    complex_sequence = target_sequence + peptide_sequence
    
    #Getting target residue-exposures
    naccess_file = OUTPUT + '/' + os.path.basename(args.target_structure).replace('.pdb', '.rsa')
    #os.system('cp ' + args.target_structure + ' ' + TMP)
    if not os.path.isfile(naccess_file):
        os.system(SCRIPTS + '/' + 'naccess ' + TMP + '/' + os.path.basename(args.target_structure) + ' ' + NACCESS)
        os.system('rm ' + os.path.basename(args.target_structure).replace('.pdb', '.log'))
        os.system('rm ' + os.path.basename(args.target_structure).replace('.pdb', '.asa'))
        os.system('mv ' + os.path.basename(args.target_structure).replace('.pdb', '.rsa') + ' ' + OUTPUT)
    with open(naccess_file, 'r') as naccess_profile:
        naccess_absolute_results = []
        naccess_results = []
        for line in naccess_profile:
            if line[:3] == 'RES':
                naccess_absolute_results.append(float(line[16:22].split()[0]))
                naccess_results.append(float(line[22:28].split()[0])/100.0)
        target = target.assign(abs_exposure = pd.Series(naccess_absolute_results))
        target = target.assign(exposure = pd.Series(naccess_results))
        
    #Getting target conservation per residue through PSI-BLAST
    target_fasta_file_name = TMP + '/' + os.path.basename(args.target_structure).replace('.pdb', '.seq')
    with open(target_fasta_file_name, 'w') as f:
        f.write('> target\n' + target_sequence)
    target_psi_results = TMP + '/' + os.path.basename(args.target_structure).replace('.pdb', '.psi')
    target_psi_msa = TMP + '/' + os.path.basename(args.target_structure).replace('.pdb', '.aln')
    #"""
    command = BLAST + '/blastpgp -a ' + str(cpus) + ' -j 3 -h 0.001 -d ' + UNIREF + ' -F F -i ' + target_fasta_file_name + ' -Q ' + target_psi_results + ' -o ' + target_psi_msa
    print command
    os.system(command)
    #"""
    target = target.assign(conservation = pd.Series([float(i) for i in os.popen("awk '{if(NF>40){print $(NF-1)}}' " + target_psi_results).readlines()]))
    
    #Getting target relative entropy per residue through HHBLITS
    target_msa = TMP + '/' + os.path.basename(args.target_structure).replace('.pdb', '.msa')
    #"""
    command = HHBLITS + '/hhblits -i ' + target_fasta_file_name + ' -id 100 -cov 70 -qid 35 -cpu ' + str(cpus) + ' -d ' + UNIPROT + ' -opsi ' + target_msa
    os.system(command)
    #"""
    target = target.assign(relative_entropy = pd.Series(relative_entropies_from_msa(target_msa)))
    
    #Predicting peptide secondary structure composition
    pept_ss_comp = sec_struct_pred(args.peptide_sequence, TMP, PSIPRED, UNIREF, BLAST, SCRIPTS)
    #feature_dict.update(dict([('ss_comp_peptide_' + str(i), j) for i, j in enumerate(pept_ss_comp)]))
    feature_dict.update({'ss_comp_peptide_0': pept_ss_comp[0]})
    
    #Making folder for results
    feature_folder = OUTPUT + '/' + os.path.basename(args.target_structure) + '__' + os.path.basename(args.peptide_sequence) + '_featfolder'
    os.system('mkdir ' + feature_folder)
    
    #Loading TMalign data
    alignments = pd.read_csv(args.alignment_pandas)
    alignments = alignments.to_dict('records')
    
    if not args.softstart:
        #Resetting feature-file
        f = open(feature_folder + '/' + os.path.basename(args.target_structure) + '__' + os.path.basename(args.peptide_sequence) + args.output_suffix, 'w')
        f.close()
    elif os.path.isfile(feature_folder + '/' + os.path.basename(args.target_structure) + '__' + os.path.basename(args.peptide_sequence) + args.output_suffix):
        #Finding out which alignments to process
        with open(feature_folder + '/' + os.path.basename(args.target_structure) + '__' + os.path.basename(args.peptide_sequence) + args.output_suffix, 'r') as f:
            processed_templates = [line.split(', ')[1] for line in f.readlines()[1:]]
            processed_templates = [template[:4] + template[7] for template in processed_templates]
            #alignments = [a for a in alignments if not a['1_interface'][:4] + a['1_interface'][7] in processed_templates]
            alignments = [a for a in alignments if not a['template'][:5] in processed_templates]
            
    #Starting work on features which can be calculated in parallell
    chunk_size = args.chunk
    current_chunk = 0
    feature_keys = []
    
    iteration_stop = len(alignments)
    if args.early_stop:
        iteration_stop = 1
    #while current_chunk < len(alignments):
    while current_chunk < iteration_stop:
        print 'Features generated for {}/{} templates'.format(current_chunk, len(alignments))
        sys.stdout.flush()
        """
        #One at a time
        features = []
        for alignment in alignments[current_chunk:min(current_chunk + chunk_size, len(alignments))]:
            features.append(workerwrapper(target, args.target_structure, fragments, complex_sequence, peptide_composition_vector, COMPDB, PDB, TMP, SCRIPTS, NACCESS, DOCKQ, interface_list, args.native, alignment))
        """
        #Multiprocessing style
        p = multiprocessing.Pool(cpus)
        func = partial(workerwrapper, target, args.target_structure, fragments, complex_sequence, peptide_composition_vector, COMPDB, PDB, TMP, SCRIPTS, NACCESS, DOCKQ, interface_list, args.native, fragment_profiles)
        features = p.map(func, alignments[current_chunk:min(current_chunk + chunk_size, len(alignments))])
        p.close()
        p.join()
        #"""
        
        #Joining features together into long list
        features = [feats for featlist in features for feats in featlist]
        
        #Discarding empty feature_dicts, these arise from sampling alignments to templates since
        #removed from the database, or from simply having poor InterComp matches between peptide
        #and complementary interface
        features = [feats for feats in features if feats]
        
        #Writing to file
        with open(feature_folder + '/' + os.path.basename(args.target_structure) + '__' + os.path.basename(args.peptide_sequence) + args.output_suffix, 'a') as out:
            for feat_nr in range(len(features)):
                features[feat_nr].update(feature_dict)
                #if current_chunk == 0 and feat_nr == 0:
                if not feature_keys:
                    feature_keys = sorted(features[feat_nr].keys())
                    if feature_keys:
                        out.write(', '.join([key for key in feature_keys]) + '\n')
                out.write(', '.join([str(features[feat_nr][key]) for key in feature_keys]) + '\n')
            
        #Updating position
        current_chunk += chunk_size
    
    #Cleaning
    os.system('rm ' + TMP + '/' + os.path.basename(args.target_structure))
    #os.system('rm ' + shell_location)
    
def relative_entropies_from_msa(msa_file):
    aa_dict = {'A': 0, 'C': 0, 'E': 0, 'D': 0, 'G': 0, 'F': 0, 'I': 0,
               'H': 0, 'K': 0, 'J': 0, 'M': 0, 'L': 0, 'N': 0, 'Q': 0,
               'P': 0, 'S': 0, 'R': 0, 'T': 0, 'W': 0, 'V': 0, 'Y': 0}
    def occurancy_probabilities(sequence):
        occur_dict = aa_dict.copy()
        for key in occur_dict:
            occur_dict[key] = (len(sequence) - len(sequence.replace(key, '')))/float(len(sequence))
        return occur_dict
    msa = []
    with open(msa_file, 'r') as msa_file_content:
        for line in msa_file_content:
            msa.append(line.split()[1])
    background_probabilities = occurancy_probabilities(''.join(msa))
    relative_entropies = []
    for position, residue in enumerate(msa[0]):
        RE = 0
        probabilities = occurancy_probabilities(''.join([seq[position] for seq in msa]))
        for aa in probabilities.keys():
            if not probabilities[aa] <= 0.0:
                RE += probabilities[aa]*math.log(probabilities[aa]/background_probabilities[aa], 2)
        relative_entropies.append(RE)
    return relative_entropies

def sec_struct_pred(peptide, TMP, PSIPRED, UNIREF, BLAST, SCRIPTS):
    peptide_base = os.path.basename(peptide)
    command = 'cp ' + peptide + ' ' + TMP + '/' + peptide_base
    os.system(command)
    if os.path.isfile(TMP + '/' + peptide_base.replace('.seq', '.mtx')):
        os.unlink(TMP + '/' + peptide_base.replace('.seq', '.mtx'))
    command = [SCRIPTS + '/' + 'create_profile.sh', TMP + '/' + peptide_base, UNIREF, '6', BLAST + '/', PSIPRED + '/']
    call(command)
    structures = {'C': 0.0, 'E': 0.0, 'H': 0.0}
    command = "awk '{print $3}' " + TMP + '/' + peptide_base.replace('.seq', '.ss2')
    lines = os.popen(command).readlines()
    for line in lines:
        if line[0] in structures:
            structures[line[0]] += 1
    os.system('rm ' + TMP + '/' + peptide_base)
    return [structures[key]/(len(lines)-2) for key in sorted(structures)]

def get_chain_length(chains_file, chain, model):
    command = "sed -nE '/MODEL        " + model + "/,/MODEL/p' " + chains_file + " | grep -e '^.\{13\}CA A.\{4\}" + chain + "' -e '^.\{13\}CA.\{6\}" + chain + "'"
    return len(os.popen(command).readlines())

def get_interface_sequence(interface_location):
    sequence = ''
    interacting_residues = []
    with open(interface_location, 'r') as f:
        for line in f.readlines():
            if line[17:20] in three_to_one.keys():
                sequence += three_to_one[line[17:20]]
            else:
                sequence += 'X'
            interacting_residues.append(int(line[22:26].split()[0]))
    return sequence, interacting_residues

def calculate_complimentarity(vector1, vector2):
    vector3 = np.dot(vector1, vector2)/(np.linalg.norm(vector1)*np.linalg.norm(vector2))
    if vector3 <= -1.0:
        return math.pi
    elif vector3 >= 1.0:
        return 0.0
    else:
        return np.arccos(vector3)

if __name__ == '__main__':
    main()
