#!/usr/bin/env python
from optparse import OptionParser
import os
import re
import pandas as pd
import argparse

def relevant_align_reader(alignments_file_name, size_minimum):
    pattern = re.compile((
        '([\S]+)[\S ]*\n'
        '([\S]+)[\S ]*\n'
        'Length of Chain_1: *(\d+) residues\n'
        'Length of Chain_2: *(\d+) residues\n\n'
        'Aligned length= *(\d+), RMSD= *([\d\.]+), Seq_ID=n_identical/n_aligned= *([\d\.]+)\n'
        'TM-score= *([\.\d]+) [\S ]+\n'
        'TM-score= *([\.\d]+) [\S ]+\n\n'
        '(\S+)\n'
        '([ \.:]+)\n'
        '(\S+)\n'
        ),
        re.MULTILINE)
    keys = ['target', 'template', 'target_length', 'template_length', 'aligned_length', 'alignment_RMSD', 'alignment_sequence_ID', 'TMscore_target', 'TMscore_template', 'TMALIGNMENT_target', 'TMALIGNMENT_key', 'TMALIGNMENT_template']
    match = []
    with open(alignments_file_name, 'r') as alignments_file:
        match = pattern.findall(alignments_file.read())
    #print match
    if not match:
        raise IOError("The alignment file is empty or improperly formatted")
    data = [list(values) for values in match]
    data = [i for i in data if float(i[3]) >= size_minimum] #Getting rid of tiny matches
    data = sorted(data, key = lambda values: max(float(values[7]), float(values[8])), reverse = True) #Previously it only took column 7 into account
    data = data[:2500]
    output_pandas = pd.DataFrame(data, columns=keys)
    return output_pandas

def main():
    #usage = '%prog [TMALIGN-FILE] [OPTIONS] ...'
    #parser = OptionParser(usage = usage)
    #(options, args) = parser.parse_args()
    parser = argparse.ArgumentParser()
    parser.add_argument('TMAligns', type = str, help = 'File of alignment results from TMAlign')
    parser.add_argument('-o', '--output', type = str, default = '{}_relevant.csv')
    parser.add_argument('-s', '--size_min', type = int, default = 25, help = 'minimum size of chain to be considered a relevant alignment')
    args = parser.parse_args()

    args.output = args.output.format(args.TMAligns)
    
    print('Looking for relevant TM-values')
    list_of_aligns = relevant_align_reader(args.TMAligns, args.size_min)
    
    print 'Writing to file'
    list_of_aligns.to_csv(os.environ["OUTPUT"] + '/' + os.path.basename(args.TMAligns) + '_relevant.csv', index=False)

if __name__ == '__main__':
    main()
