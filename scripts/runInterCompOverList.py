#!/usr/bin/env python

import multiprocessing
import os
import argparse
from functools import partial
from subprocess import call, check_output
import signal
import re

class TooLongTimeException(Exception):
    def __init__(self, value):
        self.parameter = value
    def __str__(self):
        return repr(self.parameter)

def signal_handler(signum, frame):
    raise TooLongTimeException("InterComp timed out!")

pattern = re.compile('\n.+', flags = re.DOTALL)

def worker(scripts_path, interface_path, target, partner):
    #Runs InterComp on target and partner
    output = ''
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(20)
    try:
        #output = check_output([scripts_path + '/' + 'InterfaceComparison', '-PDB', target, interface_path + '/' + partner, '-map'])
        output = check_output([scripts_path + '/' + 'InterfaceComparison', '-PDB', target, partner, '-map'])
    except TooLongTimeException as e:
        pass
    except Exception as e:
        print(e)
    finally:
        signal.alarm(0)
    if len(output) > 0:
        output = '>' + os.path.basename(target) + ' ' + os.path.basename(partner) + re.search(pattern, output).group()
    return output

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('target', type = str, help = 'searchpath to target protein chain for alignment')
    parser.add_argument('interface_list', type = str, help = 'list of interfaces to align against')
    parser.add_argument('-c', '--chunk_size', type = int, default = 1000, help = 'sets how many alignments will be done before saving data. Smaller chunk-size mean slower but safer runs')
    args = parser.parse_args()
    
    #Environment variables
    TMP = os.environ["TMP"]
    SCRIPTS = os.environ["SCRIPTS"]
    COMPDB = os.environ["COMPDB"]
    OUTPUT = os.environ["OUTPUT"]
    
    #Reads the list of alignment partners
    alignment_partners = []
    with open(args.interface_list, 'r') as alignment_partner_file:
        alignment_partners = [line.split()[0] for line in alignment_partner_file]
    #alignment_partners = alignment_partners[:50] #<-------------

    #Constructs shell of target
    shell_location = TMP + '/' + os.path.basename(args.target) + '_shell'
    os.system('cp ' + args.target + ' ' + TMP)
    call([SCRIPTS + '/' + 'naccess', os.path.basename(args.target)], cwd = TMP)
    res_exposures = []
    with open(TMP + '/' + os.path.basename(args.target).replace('.pdb', '.rsa'), 'r') as exposure_file:
        res_exposures = [float(line[22:28].split()[0]) for line in exposure_file.readlines() if line[:3] == 'RES']
        print res_exposures
    with open(TMP + '/' + os.path.basename(args.target), 'r') as original_file:
        with open(shell_location, 'w') as shell_file:
            position_counter = -1
            for line in original_file:
                if line[:4] == 'ATOM' and line[13:15] == 'CA' and line[16] in [' ', 'A']:
                    position_counter += 1
                    if res_exposures[position_counter] >= 15.0:
                        shell_file.write(line)
    os.system('rm ' + TMP + '/' + os.path.basename(args.target).replace('.pdb', '.asa'))
    os.system('rm ' + TMP + '/' + os.path.basename(args.target).replace('.pdb', '.log'))
    os.system('rm ' + TMP + '/' + os.path.basename(args.target))
    os.system('mv ' + TMP + '/' + os.path.basename(args.target).replace('.pdb', '.rsa') + ' ' + OUTPUT + '/')
    
    #Fetches amount of CPUs available
    cpus = int(os.environ["CPUS"])
    
    current_chunk = 0
    while current_chunk < len(alignment_partners):
    #while current_chunk < 1000:
        print("{} / {} alignments completed".format(current_chunk, len(alignment_partners)))
        p = multiprocessing.Pool(cpus)
        func = partial(worker, SCRIPTS, COMPDB, shell_location)
        results = p.map(func, alignment_partners[current_chunk:current_chunk+args.chunk_size])
        #Updates what chunk we're at
        current_chunk += args.chunk_size
        #Wraps up workers
        p.close()
        p.join()
        #Writes chunk results to file
        with open(os.environ["OUTPUT"] + "/" + os.path.basename(args.target) + "_alignments", 'a') as out_file:
            for line in results:
                if len(line) > 0:
                    out_file.write(line)

if __name__ == '__main__':
    main()
