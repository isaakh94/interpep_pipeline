#!/usr/bin/env python

from optparse import OptionParser
import os
import multiprocessing
from functools import partial
import re
from subprocess import check_output
import signal
import tempfile
import sys

class TooLongTimeException(Exception):
    def __init__(self, value):
        self.parameter = value
    def __str__(self):
        return repr(self.parameter)

def signal_handler(signum, frame):
    raise TooLongTimeException("TMAlign timed out!")

def worker(target, TMP, SCRIPTS, partner):
    """
    Takes a target file, a searchpath to the PDB, and a tuple of partner path and chain name.
    Runs TMAlign on with the target against the specified chain of the partner and returns it.
    """
    #Produces a single-model file of partner
    #temp_file_name = TMP + "/" + re.search('\w+\.\w+', partner[0]).group()
    temp_file_name = partner[0].split('/')[-1]
    temp_file_name_original = temp_file_name[:4] + partner[1] + '.pdb_temp'
    temp_file = tempfile.NamedTemporaryFile(delete = False, dir = TMP)
    temp_file.close()
    temp_file_name = temp_file.name
    #command = 'sed "/ENDMDL/q" ' + searchpath + partner + ' > ' + temp_file_name
    #command = 'sed "/ENDMDL/q" ' + partner[0] + ' | grep "^ATOM.\{17\}' + partner[1] + '"' + ' > ' + temp_file_name
    command = 'sed -n "/MODEL        ' + partner[2] + '/,/ENDMDL/p" ' + partner[0] + ' | grep "^ATOM.\{17\}' + partner[1] + '"' + ' > ' + temp_file_name
    os.system(command)
    #Runs TMAlignC++ on target vs. partner
    output = ''
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(10)
    try:
        output = check_output([SCRIPTS + '/' + 'TMAlignC++', '-A', target, '-B', temp_file_name])
    except TooLongTimeException as e:
        pass
    except Exception as e:
        print('Error aligning to {}_{}:'.format(partner[0], partner[1]))
        print(e)
    finally:
        signal.alarm(0)
    #Replaces long searchpaths with names of actual files
    output = output.replace(target, os.path.basename(target))
    output = output.replace(temp_file_name, os.path.basename(temp_file_name_original))
    #Removes temporary file
    os.system('rm ' + temp_file_name)
    return output

def main():
    #Handles args and options
    usage = '%prog [TARGET] [PDB-LIST] [OPTIONS] ...'
    parser = OptionParser(usage = usage)
    parser.add_option('-c', '--chunksize', type = 'int', default = 160, help = 'Sets how many alignments will be made between each save of data, a lower chunk-size is safer but slower', dest = 'chunk_size')
    parser.add_option('-v', '--verbose', action='store_true', default=False, help='Prints extra status messages')
    (options, args) = parser.parse_args()
    if len(args) < 2:
        exit('ERROR: Two files required to run runTMalignOverList.py')
        
    PDB = os.environ["PDBBIO"]
    TMP = os.environ["TMP"]
    SCRIPTS = os.environ["SCRIPTS"]
    
    print("==(1)== PERFORMING STRUCTURAL ALIGNMENTS ==(1)==")
    
    #Reads the PDB-LIST file of what to align against
    alignment_partners = []
    with open(args[1], 'r') as paths_to_partners_file:
        alignment_partners = [line.split() for line in paths_to_partners_file.readlines()]
    
    #Establishes the amount of resources available
    cpus = int(os.environ["CPUS"])
    
    #Opens the output-file
    alignment_file_path = os.environ["OUTPUT"] + "/" + os.path.basename(args[0]) + "_alignments"
    alignment_file = open(alignment_file_path, 'w')
    
    chunk_size = options.chunk_size
    current_chunk = 0
    
    while current_chunk < len(alignment_partners):
        if options.verbose:
            print("{} / {} alignments completed".format(current_chunk, len(alignment_partners)))
        #Starts up workers for the multiprocessing and hands them the chunk of work
        p = multiprocessing.Pool(cpus)
        func = partial(worker, args[0], TMP, SCRIPTS)
        results = p.map(func, alignment_partners[current_chunk:current_chunk+chunk_size])
        #Updates what chunk we're at
        current_chunk += chunk_size
        #Wraps up the workers
        p.close()
        p.join()
        #Writes results from this chunk to file, in order
        for line in results:
            if len(line) > 0:
                alignment_file.write(line)
    
    alignment_file.close()

if __name__ == '__main__':
    main()
