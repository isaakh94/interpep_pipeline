#!/usr/bin/env python
import os

################## InterPep2 CONFIGURATION FILE ###################

####### Please set the paths below before running InterPep2 #######

# Location of your preferred tmp folder
#os.environ["TMP"] = "/proj/wallner/users/x_isaak/pepip_pipeline/tmp"
os.environ["TMP"] = os.environ["SNIC_TMP"]

# Location of biounit PDB
#os.environ["PDBBIO"] = "/proj/wallner/users/x_clami/PDB/19052016_biounit"
os.environ["PDBBIO"] = "/proj/wallner/share/PDB/191015_biounit"

# Location of the InterComp interaction templates
#os.environ["COMPDB"] = "/proj/wallner/users/x_clami/interface_comparison/interface_comparison_nullmatching/interface_comparison/interfaces_bio_models_5"
os.environ["COMPDB"] = "/proj/wallner/share/PDB/191015_biounit_interfaces"

# Location of your local copy of Uniref
os.environ["UNIREF"] = "/proj/wallner/users/x_isaak/DB/uniref90.fasta"

# Location of your BLAST installation
os.environ["BLAST"] = "/proj/wallner/users/x_bjowa/github/ProQ_scripts/apps/blast-2.2.18_x86_64/bin"

# Location of your PSIPRED installation
os.environ["PSIPRED"] = "/proj/wallner/users/x_bjowa/github/ProQ_scripts/apps/psipred25/bin"

# Location of your local hhblits installation
os.environ["HHBLITS"] = "/proj/wallner/users/x_clami/hhsuite-2.0.15/bin"

# Location of local clustered UNIPROT
os.environ["UNIPROT"] = "/proj/wallner/users/x_clami/uniprot/uniprot20_2016_02"

# Location of your local Rosetta installation
os.environ["ROSETTA"] = "/proj/wallner/apps/rosetta/Rosetta"

# Location of your local naccess installation
os.environ["NACCESS"] = "/proj/wallner/users/x_bjowa/local/naccess"

# Location of local DockQ
os.environ["DOCKQ"] = "/proj/wallner/users/x_isaak/DockQ"
