#!/usr/bin/env python

import os
import re
from glob import glob
import commands
import argparse
import sys

def make_PDB_cache_file(path_to_partners, interfaces_list, PDB):
    """
    Uses list of all interfaces to build library of which PDB-files to run against
    """
    dict_of_chains = {}
    with open(interfaces_list, 'r') as interfaces:
        for interface in interfaces:
            interface_name = interface.split('/')[-1]
            if not len(interface_name) == 15:
                continue
            try:
                existing_entries = dict_of_chains[interface_name[:4]]
                if not interface_name[7] in [i[-1] for i in existing_entries]:
                    dict_of_chains[interface_name[:4]].append(interface_name[4] + interface_name[6:8])
            except:
                dict_of_chains[interface_name[:4]] = [interface_name[4] + interface_name[6:8]]
    
    with open(path_to_partners, 'w') as f:
        for entry in sorted(dict_of_chains.keys()):
            for chain in dict_of_chains[entry]:
                f.write(PDB + '/' + entry[1:3] + '/' + entry + chain[0] + '.pdb ' + chain[2] + ' ' + chain[1] + '\n')

def make_InterComp_cache_file(interfaces_list, COMPDB):
    subdirs = True
    os.system('ls ' + COMPDB + '/*/ | grep ".pdb$" > ' + interfaces_list)
    if int(os.popen('wc -l ' + interfaces_list).read().split()[0]) <= 1:
        subdirs = False
        os.system('ls ' + COMPDB + '/ | grep ".pdb$" > ' + interfaces_list)
    all_interfaces = []
    with open(interfaces_list, 'r') as f:
        all_interfaces = f.readlines()
    if subdirs:
        all_interfaces = [COMPDB + '/' + interface[1:3] + '/' + interface for interface in all_interfaces]
    else:
        all_interfaces = [COMPDB + '/' + interface for interface in all_interfaces]
    with open(interfaces_list, 'w') as f:
        f.write(''.join(all_interfaces))
                
def prepare_for_struct_align(path_to_partners, interfaces_list):
    """
    Checks for a list of all potential template chains within the PDB.
    If no such list exists, it makes one.
    """
    try:
        DB = os.environ["DB"]
        PDB = os.environ["PDBBIO"]
        COMPDB = os.environ["COMPDB"]
    except KeyError:
        print('Environment variable not found.')

    if not os.path.isfile(interfaces_list):
        print('     There is no cache of InterComp Interfaces in the databases folder.')
        print('     Building new internal cache of interfaces (this may take a while).')
        make_InterComp_cache_file(interfaces_list, COMPDB)
        print('     Caching complete!')
    if not os.path.isfile(path_to_partners):
        print('     There is no cache of PDB interacting chains in the databases folder.')
        print('     Building new internal cache of interacting chains.')
        make_PDB_cache_file(path_to_partners, interfaces_list, PDB)
        print('     Caching complete!')
    if os.path.getmtime(path_to_partners) < os.path.getmtime(PDB):
        print("     The internal file cache appears to be outdated.\n     If a new PDB database is in use, updating the cache file is highly recommended.\n     To do so, delete file " + path_to_partners + " and restart InterPep")

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('target_structure', type = str, help = 'Path to file of target structure')
    parser.add_argument('peptide_sequence', type = str, help = 'Path to file of peptide sequence')
    parser.add_argument('-c', '--cpu', type = int, default = 1, help = 'The number of cpus InterPep is allowed to use')
    parser.add_argument('-m', '--model', type = float, default = -1.0, help = 'The sequence identity between model template and native sequence. Only specify this flag if you are running on a non-native structure.')
    parser.add_argument('-n', '--native', type = str, default = '', help = 'path to native file for the complex. If this is set, DockQ-metrics are calculated. For benchmarking')
    parser.add_argument('-o', '--output', type = str, default = '', help = 'location of output-directory')
    parser.add_argument('-d', '--database', type = str, default = '', help = 'Use if you want to use another database of templates than the paths_to_partners one')
    parser.add_argument('--compdbcache', type = str, default = '', help = 'Use if you want to use another list of InterComp interfaces than interfaces_full_list')
    parser.add_argument('-s', '--steps', type = int, nargs = '*', default = [0, 1, 2, 3, 4, 5], choices = range(6), help = 'Which steps of InterPep to run. 0=precheck, 1=alignments, 2=fragment_generation, 3=feature_generation, 4=random_forest, 5=predictions')
    parser.add_argument('-p', '--precalculated', action = 'store_true', help = 'set if there are already pre-calculated entries in the feature-file (such as upon a crash)')
    args = parser.parse_args()
    
    print("Initiating InterPep")
    sys.stdout.flush()

    # Fetches absolute path to where the InterPep.py file is located
    #os.environ["BASE"] = os.path.dirname(os.path.realpath(__file__))
    os.environ["BASE"] = "/proj/wallner/users/x_isaak/pepip_pipeline"
    BASE = os.environ["BASE"]
    
    # Initiates other paths
    sys.path.append(BASE + '/')
    import configure
    
    os.environ["DB"] = BASE + "/databases"
    DB = os.environ["DB"]
    os.environ["SCRIPTS"] = BASE + "/scripts"
    SCRIPTS = BASE + "/scripts"
    #os.environ["OUTPUT"] = BASE + "/output"
    if args.output == '':
        args.output = BASE + "/output"
    os.environ["OUTPUT"] = args.output
    OUTPUT = os.environ["OUTPUT"]
    os.environ["CPUS"] = str(args.cpu)

    if not args.peptide_sequence.split('.')[-1] == '.seq':
        os.system('cp ' + args.peptide_sequence + ' ' + OUTPUT + '/' + '.'.join(os.path.basename(args.peptide_sequence).split('.')[:-1]) + '.seq')
        args.peptide_sequence = OUTPUT + '/' + '.'.join(os.path.basename(args.peptide_sequence).split('.')[:-1]) + '.seq'
    
    target_name = os.path.basename(args.target_structure)
    peptide_name = os.path.basename(args.peptide_sequence)

    if not args.database:
        args.database = DB + '/paths_to_partners'
    if not args.compdbcache:
        args.compdbcache = DB + '/interfaces_full_list'
    
    #Step 0, checking if we have a cached PDB database available
    if 0 in args.steps:
        prepare_for_struct_align(args.database, args.compdbcache)
    
    #Step 1, performing structural alignments to find possible templates
    if 1 in args.steps:
        if not args.database:
            os.system(SCRIPTS + '/' + 'runTMalignOverList.py ' + args.target_structure + ' ' + DB + '/' + 'paths_to_partners -v')
        else:
            os.system(SCRIPTS + '/' + 'runTMalignOverList.py ' + args.target_structure + ' ' + args.database + ' -v')
        os.system(SCRIPTS + '/' + 'alignment_processor.py ' + OUTPUT + '/' + target_name + '_alignments')

    #Step 2, Constructing rigid Peptide fragments
    if 2 in args.steps:
        os.system(SCRIPTS + '/' + 'make_fragments.py ' + args.peptide_sequence)

    #Step 3, extracting features from aligned fragment complexes
    if 3 in args.steps:
        extra_args = ''
        if args.precalculated:
            extra_args = extra_args + ' -s'
        if args.native:
            os.system(SCRIPTS + '/' + 'fragment_feature_generator.py ' + args.target_structure + ' ' + OUTPUT + '/' + target_name + '_alignments_relevant.csv' + ' ' + args.peptide_sequence + ' ' + '--native ' + args.native + extra_args)
        else:
            os.system(SCRIPTS + '/' + 'fragment_feature_generator.py ' + args.target_structure + ' ' + OUTPUT + '/' + target_name + '_alignments_relevant.csv' + ' ' + args.peptide_sequence + extra_args)
    
    #Step 4, applying the random forest
    if 4 in args.steps:
        os.system(SCRIPTS + '/' + 'random_forest_analysis.py ' + OUTPUT + '/' + target_name + '__' + peptide_name + '_featfolder/' + target_name + '__' + peptide_name + '_interpepfeatures ' + DB + '/' + 'InterPep2_RandomForest.pkl')
    
    #Step 5, clustering and producing final predictions
    if 5 in args.steps:
        os.system('python ' + SCRIPTS + '/' + 'predict.py ' + args.target_structure + ' ' + OUTPUT + '/' + os.path.basename(args.peptide_sequence) + '_fragfolder/' + ' ' + OUTPUT + '/' + os.path.basename(OUTPUT + '/' + target_name + '__' + peptide_name + '_featfolder/' + target_name + '__' + peptide_name + '_interpepfeatures').replace('features', '') + '_predictions/randomforestoutput.csv')

if __name__ == '__main__':
    main()
